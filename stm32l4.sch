EESchema Schematic File Version 4
LIBS:L80RE_Lora_murata-cache
LIBS:gps_l80-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 600813FA
P 4050 4200
AR Path="/600813FA" Ref="R?"  Part="1" 
AR Path="/600048E7/600813FA" Ref="R?"  Part="1" 
AR Path="/6005F2C2/600813FA" Ref="R20"  Part="1" 
F 0 "R20" V 4257 4200 50  0000 C CNN
F 1 "100K 0402" V 4166 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3980 4200 50  0001 C CNN
F 3 "~" H 4050 4200 50  0001 C CNN
	1    4050 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 4200 3900 4200
$Comp
L power:GND #PWR?
U 1 1 60081414
P 3550 2900
AR Path="/60081414" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60081414" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60081414" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 3550 2650 50  0001 C CNN
F 1 "GND" H 3555 2727 50  0000 C CNN
F 2 "" H 3550 2900 50  0001 C CNN
F 3 "" H 3550 2900 50  0001 C CNN
	1    3550 2900
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Reed SW?
U 1 1 600814BB
P 9100 1700
AR Path="/600814BB" Ref="SW?"  Part="1" 
AR Path="/600048E7/600814BB" Ref="SW?"  Part="1" 
AR Path="/6005F2C2/600814BB" Ref="SW1"  Part="1" 
F 0 "SW1" H 9100 1922 50  0000 C CNN
F 1 "SW_Reed" H 9100 1831 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_REED_CT10-XXXX-G1" H 9100 1700 50  0001 C CNN
F 3 "~" H 9100 1700 50  0001 C CNN
	1    9100 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 600814C1
P 9550 1550
AR Path="/600814C1" Ref="R?"  Part="1" 
AR Path="/600048E7/600814C1" Ref="R?"  Part="1" 
AR Path="/6005F2C2/600814C1" Ref="R26"  Part="1" 
F 0 "R26" H 9480 1504 50  0000 R CNN
F 1 "4.7K 0402" H 9480 1595 50  0000 R CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9480 1550 50  0001 C CNN
F 3 "~" H 9550 1550 50  0001 C CNN
	1    9550 1550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600814C7
P 8650 1750
AR Path="/600814C7" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/600814C7" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/600814C7" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 8650 1500 50  0001 C CNN
F 1 "GND" H 8655 1577 50  0000 C CNN
F 2 "" H 8650 1750 50  0001 C CNN
F 3 "" H 8650 1750 50  0001 C CNN
	1    8650 1750
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 600814CD
P 9550 2000
AR Path="/600814CD" Ref="C?"  Part="1" 
AR Path="/600048E7/600814CD" Ref="C?"  Part="1" 
AR Path="/6005F2C2/600814CD" Ref="C62"  Part="1" 
F 0 "C62" H 9728 2046 50  0000 L CNN
F 1 "0.1uF 0402" H 9728 1955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9550 2000 50  0001 C CNN
F 3 "~" H 9550 2000 50  0001 C CNN
	1    9550 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600814D3
P 9550 2350
AR Path="/600814D3" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/600814D3" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/600814D3" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 9550 2100 50  0001 C CNN
F 1 "GND" H 9555 2177 50  0000 C CNN
F 2 "" H 9550 2350 50  0001 C CNN
F 3 "" H 9550 2350 50  0001 C CNN
	1    9550 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2250 9550 2350
Wire Wire Line
	9300 1700 9550 1700
Wire Wire Line
	9550 1700 9550 1750
Connection ~ 9550 1700
Wire Wire Line
	8900 1700 8650 1700
Wire Wire Line
	8650 1700 8650 1750
Wire Wire Line
	10500 1550 10500 1700
Wire Wire Line
	9550 1700 10500 1700
$Comp
L Device:R R?
U 1 1 6008150D
P 2600 1400
AR Path="/6008150D" Ref="R?"  Part="1" 
AR Path="/600048E7/6008150D" Ref="R?"  Part="1" 
AR Path="/6005F2C2/6008150D" Ref="R18"  Part="1" 
F 0 "R18" V 2807 1400 50  0000 C CNN
F 1 "4.7K 0402" V 2716 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2530 1400 50  0001 C CNN
F 3 "~" H 2600 1400 50  0001 C CNN
	1    2600 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 1150 3850 1250
Wire Wire Line
	2200 1200 2200 1400
Wire Wire Line
	2200 1400 2450 1400
$Comp
L Device:R R?
U 1 1 6008152B
P 8300 4100
AR Path="/6008152B" Ref="R?"  Part="1" 
AR Path="/600048E7/6008152B" Ref="R?"  Part="1" 
AR Path="/6005F2C2/6008152B" Ref="R23"  Part="1" 
F 0 "R23" V 8507 4100 50  0000 C CNN
F 1 "4.7K 0402" V 8416 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8230 4100 50  0001 C CNN
F 3 "~" H 8300 4100 50  0001 C CNN
	1    8300 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60081531
P 9050 4100
AR Path="/60081531" Ref="R?"  Part="1" 
AR Path="/600048E7/60081531" Ref="R?"  Part="1" 
AR Path="/6005F2C2/60081531" Ref="R25"  Part="1" 
F 0 "R25" V 9257 4100 50  0000 C CNN
F 1 "4.7K 0402" V 9166 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8980 4100 50  0001 C CNN
F 3 "~" H 9050 4100 50  0001 C CNN
	1    9050 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3450 8800 3450
Wire Wire Line
	8050 3550 8600 3550
Wire Wire Line
	8800 3450 8800 4100
Wire Wire Line
	8600 3550 8600 4100
Wire Wire Line
	8900 4100 8800 4100
Wire Wire Line
	8450 4100 8600 4100
Wire Wire Line
	9250 4100 9200 4100
$Comp
L Device:R R?
U 1 1 6008158A
P 8500 1100
AR Path="/6008158A" Ref="R?"  Part="1" 
AR Path="/600048E7/6008158A" Ref="R?"  Part="1" 
AR Path="/6005F2C2/6008158A" Ref="R24"  Part="1" 
F 0 "R24" H 8430 1054 50  0000 R CNN
F 1 "4.7K 0402" H 8430 1145 50  0000 R CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8430 1100 50  0001 C CNN
F 3 "~" H 8500 1100 50  0001 C CNN
	1    8500 1100
	-1   0    0    1   
$EndComp
$Comp
L pspice:CAP C?
U 1 1 60081590
P 8500 1550
AR Path="/60081590" Ref="C?"  Part="1" 
AR Path="/600048E7/60081590" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60081590" Ref="C56"  Part="1" 
F 0 "C56" H 8678 1596 50  0000 L CNN
F 1 "0.1uF 0402" H 8678 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8500 1550 50  0001 C CNN
F 3 "~" H 8500 1550 50  0001 C CNN
	1    8500 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 1250 8500 1250
Wire Wire Line
	8500 1250 8500 1300
Connection ~ 8500 1250
Wire Wire Line
	8500 800  8500 950 
Wire Wire Line
	8650 1700 8550 1700
Wire Wire Line
	8550 1700 8550 1800
Wire Wire Line
	8550 1800 8500 1800
Connection ~ 8650 1700
Wire Wire Line
	7650 1250 7650 1750
Wire Wire Line
	7650 1750 8400 1750
Wire Wire Line
	8400 1750 8400 1800
Wire Wire Line
	8400 1800 8500 1800
Connection ~ 8500 1800
Wire Wire Line
	9550 1350 9550 1400
Wire Wire Line
	3700 2850 3700 3000
$Comp
L mems_mic_board-cache:MP34DT06JTR MK?
U 1 1 60081606
P 2350 4350
AR Path="/60081606" Ref="MK?"  Part="1" 
AR Path="/600048E7/60081606" Ref="MK?"  Part="1" 
AR Path="/6005F2C2/60081606" Ref="MK1"  Part="1" 
F 0 "MK1" H 2350 4817 50  0000 C CNN
F 1 "MP34DT06JTR" H 2350 4726 50  0000 C CNN
F 2 "MIC_MP34DT06JTR" H 2350 4350 50  0001 L BNN
F 3 "" H 2350 4350 50  0001 L BNN
F 4 "1.1mm" H 2350 4350 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 5 "2" H 2350 4350 50  0001 L BNN "PARTREV"
F 6 "STMicroelectronics" H 2350 4350 50  0001 L BNN "MANUFACTURER"
F 7 "Manufacturer Recommendation" H 2350 4350 50  0001 L BNN "STANDARD"
	1    2350 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 60116EFE
P 3250 1400
F 0 "D4" H 3243 1616 50  0000 C CNN
F 1 "LED" H 3243 1525 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3250 1400 50  0001 C CNN
F 3 "~" H 3250 1400 50  0001 C CNN
	1    3250 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1400 3400 1250
Wire Wire Line
	3400 1250 3850 1250
Wire Wire Line
	2750 1400 3100 1400
Wire Wire Line
	2950 4150 3250 4150
$Comp
L power:GND #PWR?
U 1 1 602A1B94
P 1200 4350
AR Path="/602A1B94" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/602A1B94" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/602A1B94" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 1200 4100 50  0001 C CNN
F 1 "GND" H 1205 4177 50  0000 C CNN
F 2 "" H 1200 4350 50  0001 C CNN
F 3 "" H 1200 4350 50  0001 C CNN
	1    1200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4350 1400 4350
$Comp
L power:GND #PWR?
U 1 1 602AC23D
P 3100 4650
AR Path="/602AC23D" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/602AC23D" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/602AC23D" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 3100 4400 50  0001 C CNN
F 1 "GND" H 3105 4477 50  0000 C CNN
F 2 "" H 3100 4650 50  0001 C CNN
F 3 "" H 3100 4650 50  0001 C CNN
	1    3100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4650 3100 4650
Wire Wire Line
	1750 5000 1750 4450
Wire Wire Line
	2950 4350 3900 4350
$Comp
L pspice:CAP C?
U 1 1 602CAB27
P 1200 4000
AR Path="/602CAB27" Ref="C?"  Part="1" 
AR Path="/600048E7/602CAB27" Ref="C?"  Part="1" 
AR Path="/6005F2C2/602CAB27" Ref="C36"  Part="1" 
F 0 "C36" H 1022 3954 50  0000 R CNN
F 1 "0.1uF 0402" H 1022 4045 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1200 4000 50  0001 C CNN
F 3 "~" H 1200 4000 50  0001 C CNN
	1    1200 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 3750 3250 3750
Wire Wire Line
	3250 3750 3250 4150
Wire Wire Line
	1200 4250 1200 4350
Connection ~ 1200 4350
$Comp
L pspice:CAP C?
U 1 1 602E43F2
P 950 5650
AR Path="/602E43F2" Ref="C?"  Part="1" 
AR Path="/600048E7/602E43F2" Ref="C?"  Part="1" 
AR Path="/6005F2C2/602E43F2" Ref="C35"  Part="1" 
F 0 "C35" H 772 5604 50  0000 R CNN
F 1 "5.6pF 0402" H 772 5695 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 950 5650 50  0001 C CNN
F 3 "~" H 950 5650 50  0001 C CNN
	1    950  5650
	-1   0    0    1   
$EndComp
$Comp
L pspice:CAP C?
U 1 1 602EACA4
P 1900 5700
AR Path="/602EACA4" Ref="C?"  Part="1" 
AR Path="/600048E7/602EACA4" Ref="C?"  Part="1" 
AR Path="/6005F2C2/602EACA4" Ref="C38"  Part="1" 
F 0 "C38" H 1722 5654 50  0000 R CNN
F 1 "5.6pF 0402" H 1722 5745 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1900 5700 50  0001 C CNN
F 3 "~" H 1900 5700 50  0001 C CNN
	1    1900 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	950  6200 950  5900
Wire Wire Line
	1400 6000 1400 5400
Connection ~ 1400 4350
Wire Wire Line
	1400 4350 1200 4350
$Comp
L power:GND #PWR?
U 1 1 60311734
P 1400 6500
AR Path="/60311734" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60311734" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60311734" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 1400 6250 50  0001 C CNN
F 1 "GND" H 1405 6327 50  0000 C CNN
F 2 "" H 1400 6500 50  0001 C CNN
F 3 "" H 1400 6500 50  0001 C CNN
	1    1400 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6400 1400 6500
Wire Wire Line
	950  5400 1400 5400
Connection ~ 1400 5400
Wire Wire Line
	1400 5400 1400 4350
Wire Wire Line
	1400 5400 1900 5400
Wire Wire Line
	1900 5400 1900 5450
Wire Wire Line
	1900 5950 1900 6200
Wire Wire Line
	950  6200 950  6900
Wire Wire Line
	950  6900 2200 6900
Wire Wire Line
	2200 6900 2200 6300
Connection ~ 950  6200
$Comp
L power:GND #PWR?
U 1 1 60354B5B
P 3750 4200
AR Path="/60354B5B" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60354B5B" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60354B5B" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 3750 3950 50  0001 C CNN
F 1 "GND" H 3755 4027 50  0000 C CNN
F 2 "" H 3750 4200 50  0001 C CNN
F 3 "" H 3750 4200 50  0001 C CNN
	1    3750 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60364B56
P 4450 3700
AR Path="/60364B56" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60364B56" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60364B56" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 4450 3450 50  0001 C CNN
F 1 "GND" H 4455 3527 50  0000 C CNN
F 2 "" H 4450 3700 50  0001 C CNN
F 3 "" H 4450 3700 50  0001 C CNN
	1    4450 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3600 4450 3700
$Comp
L pspice:CAP C?
U 1 1 603969DB
P 3450 2250
AR Path="/603969DB" Ref="C?"  Part="1" 
AR Path="/600048E7/603969DB" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603969DB" Ref="C40"  Part="1" 
F 0 "C40" H 3628 2296 50  0000 L CNN
F 1 "0.1uF 0402" H 3628 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3450 2250 50  0001 C CNN
F 3 "~" H 3450 2250 50  0001 C CNN
	1    3450 2250
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603ACA4A
P 4300 2250
AR Path="/603ACA4A" Ref="C?"  Part="1" 
AR Path="/600048E7/603ACA4A" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603ACA4A" Ref="C41"  Part="1" 
F 0 "C41" H 4478 2296 50  0000 L CNN
F 1 "0.1uF 0402" H 4478 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4300 2250 50  0001 C CNN
F 3 "~" H 4300 2250 50  0001 C CNN
	1    4300 2250
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603B3CCE
P 5150 2200
AR Path="/603B3CCE" Ref="C?"  Part="1" 
AR Path="/600048E7/603B3CCE" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603B3CCE" Ref="C45"  Part="1" 
F 0 "C45" H 5328 2246 50  0000 L CNN
F 1 "0.1uF 0402" H 5328 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5150 2200 50  0001 C CNN
F 3 "~" H 5150 2200 50  0001 C CNN
	1    5150 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603BB006
P 5900 2200
AR Path="/603BB006" Ref="C?"  Part="1" 
AR Path="/600048E7/603BB006" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603BB006" Ref="C46"  Part="1" 
F 0 "C46" H 6078 2246 50  0000 L CNN
F 1 "0.1uF 0402" H 6078 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5900 2200 50  0001 C CNN
F 3 "~" H 5900 2200 50  0001 C CNN
	1    5900 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603C225D
P 6700 2200
AR Path="/603C225D" Ref="C?"  Part="1" 
AR Path="/600048E7/603C225D" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603C225D" Ref="C48"  Part="1" 
F 0 "C48" H 6878 2246 50  0000 L CNN
F 1 "0.1uF 0402" H 6878 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6700 2200 50  0001 C CNN
F 3 "~" H 6700 2200 50  0001 C CNN
	1    6700 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603D0FA0
P 2650 2200
AR Path="/603D0FA0" Ref="C?"  Part="1" 
AR Path="/600048E7/603D0FA0" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603D0FA0" Ref="C39"  Part="1" 
F 0 "C39" H 2828 2246 50  0000 L CNN
F 1 "0.1uF 0402" H 2828 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2650 2200 50  0001 C CNN
F 3 "~" H 2650 2200 50  0001 C CNN
	1    2650 2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 603D81CF
P 1850 2200
AR Path="/603D81CF" Ref="C?"  Part="1" 
AR Path="/600048E7/603D81CF" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603D81CF" Ref="C37"  Part="1" 
F 0 "C37" H 2028 2246 50  0000 L CNN
F 1 "0.1uF 0402" H 2028 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1850 2200 50  0001 C CNN
F 3 "~" H 1850 2200 50  0001 C CNN
	1    1850 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2450 5900 2450
Wire Wire Line
	5900 2450 5150 2450
Connection ~ 5900 2450
Wire Wire Line
	4300 2500 5150 2500
Wire Wire Line
	5150 2500 5150 2450
Connection ~ 5150 2450
Wire Wire Line
	3450 2500 4300 2500
Connection ~ 4300 2500
Wire Wire Line
	2650 2450 3400 2450
Wire Wire Line
	3400 2450 3400 2500
Wire Wire Line
	3400 2500 3450 2500
Connection ~ 3450 2500
Wire Wire Line
	800  2450 1850 2450
Wire Wire Line
	2650 2450 1850 2450
Connection ~ 2650 2450
Connection ~ 1850 2450
Wire Wire Line
	1850 1950 2650 1950
Wire Wire Line
	2650 1950 3450 1950
Wire Wire Line
	3450 1950 3450 2000
Connection ~ 2650 1950
Wire Wire Line
	3450 1950 4300 1950
Wire Wire Line
	4300 1950 4300 2000
Connection ~ 3450 1950
Connection ~ 4300 1950
Wire Wire Line
	4300 1950 5150 1950
Wire Wire Line
	5150 1950 5900 1950
Connection ~ 5150 1950
Wire Wire Line
	5900 1950 6700 1950
Connection ~ 5900 1950
Wire Wire Line
	5900 2450 5900 2650
$Comp
L power:GND #PWR?
U 1 1 604A62E2
P 7150 1950
AR Path="/604A62E2" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/604A62E2" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/604A62E2" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 7150 1700 50  0001 C CNN
F 1 "GND" H 7155 1777 50  0000 C CNN
F 2 "" H 7150 1950 50  0001 C CNN
F 3 "" H 7150 1950 50  0001 C CNN
	1    7150 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1950 6950 1950
Connection ~ 6700 1950
Text HLabel 5850 2650 0    50   Input ~ 0
3V3
Wire Wire Line
	5850 2650 5900 2650
Connection ~ 5900 2650
Text HLabel 3250 3750 2    50   Input ~ 0
3V3E
Text HLabel 5250 3050 1    50   Input ~ 0
UART1_TX
Text HLabel 5150 3050 1    50   Input ~ 0
UART1_RX
Text HLabel 5650 3150 1    50   Input ~ 0
NSS
Text HLabel 5550 3150 1    50   Input ~ 0
SCK
Text HLabel 5350 3150 1    50   Input ~ 0
MOSI
Text HLabel 5450 3150 1    50   Input ~ 0
MISO
Text HLabel 6550 3900 2    50   Input ~ 0
SX_NRESET
Text HLabel 6500 4700 2    50   Input ~ 0
BUSY
Text HLabel 6850 4500 2    50   Input ~ 0
DIO1
Text Label 8050 3450 2    50   ~ 0
I2C_SDA
Text Label 8050 3550 2    50   ~ 0
I2C_SCL
Wire Wire Line
	5350 3150 5350 3200
Wire Wire Line
	5450 3200 5450 3150
Wire Wire Line
	5250 3200 5250 3050
Wire Wire Line
	5150 3200 5150 3050
Wire Wire Line
	4550 4400 4650 4400
Text HLabel 8100 3950 0    50   Input ~ 0
3V3
Wire Wire Line
	8100 3950 8150 3950
Wire Wire Line
	8150 3950 8150 4100
Wire Wire Line
	4600 4700 4650 4700
Wire Wire Line
	4600 4800 4650 4800
Text Label 10500 1550 0    50   ~ 0
Reed_Switch
Text Label 9200 1250 0    50   ~ 0
Button
Wire Wire Line
	8500 1250 9200 1250
Text Label 5350 5850 3    50   ~ 0
Reed_Switch
Text Label 4600 4700 2    50   ~ 0
Button
Text HLabel 8500 800  0    50   Input ~ 0
3V3
Text HLabel 9550 1350 2    50   Input ~ 0
3V3
Text HLabel 9200 3900 0    50   Input ~ 0
3V3
Wire Wire Line
	9250 3900 9200 3900
Wire Wire Line
	9250 3900 9250 4100
Text Label 5950 3150 1    50   ~ 0
SWDIO
Text Label 5750 3150 1    50   ~ 0
SWCLK
Text Label 4450 3100 2    50   ~ 0
NRST
Wire Wire Line
	3700 2850 5900 2850
Wire Wire Line
	5900 2650 5900 2850
Text Label 3800 3350 0    50   ~ 0
SWDIO
Text Label 3800 3450 0    50   ~ 0
SWCLK
$Comp
L STM32WB55CGU6:STM32WB55CGU6 IC1
U 1 1 60A717B9
P 4650 3900
F 0 "IC1" H 6494 3346 50  0000 L CNN
F 1 "STM32WB55CGU6" H 6494 3255 50  0000 L CNN
F 2 "QFN50P700X700X65-49N-D" H 6300 4400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/STM32WB55CGU6.pdf" H 6300 4300 50  0001 L CNN
F 4 "ARM Microcontrollers - MCU Ultra-low-power dual core Arm Cortex-M4 MCU 64 MHz, Cortex-M0 32MHz with 1 Mbyte Flash, BLE, 802.15.4, USB, LCD, AES-256" H 6300 4200 50  0001 L CNN "Description"
F 5 "0.65" H 6300 4100 50  0001 L CNN "Height"
F 6 "511-STM32WB55CGU6" H 6300 4000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-STM32WB55CGU6" H 6300 3900 50  0001 L CNN "Mouser Price/Stock"
F 8 "STMicroelectronics" H 6300 3800 50  0001 L CNN "Manufacturer_Name"
F 9 "STM32WB55CGU6" H 6300 3700 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4300 4650 4300
Text Label 4550 4300 2    50   ~ 0
I2C_SCL
Text Label 4550 4400 2    50   ~ 0
I2C_SDA
Wire Wire Line
	4200 4200 4650 4200
$Comp
L L80RE_Lora_murata-rescue:HDC2010YPAT-stm32wb_lora_EU_card-rescue U5
U 1 1 60AD6189
P 10450 6150
F 0 "U5" H 10250 6625 50  0000 C CNN
F 1 "HDC2010YPAT" H 10250 6534 50  0000 C CNN
F 2 "stm32wb_lora_EU_card444:BGA6C100X50P2X3_146X146X67" H 10450 6150 50  0001 C CNN
F 3 "" H 10450 6150 50  0001 C CNN
	1    10450 6150
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 60AD99F9
P 10850 5900
AR Path="/60AD99F9" Ref="C?"  Part="1" 
AR Path="/600048E7/60AD99F9" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60AD99F9" Ref="C63"  Part="1" 
F 0 "C63" H 10672 5854 50  0000 R CNN
F 1 "0.1uF 0402" H 10672 5945 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10850 5900 50  0001 C CNN
F 3 "~" H 10850 5900 50  0001 C CNN
	1    10850 5900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60AD99FF
P 11100 5650
AR Path="/60AD99FF" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60AD99FF" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60AD99FF" Ref="#PWR0148"  Part="1" 
F 0 "#PWR0148" H 11100 5400 50  0001 C CNN
F 1 "GND" H 11105 5477 50  0000 C CNN
F 2 "" H 11100 5650 50  0001 C CNN
F 3 "" H 11100 5650 50  0001 C CNN
	1    11100 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	11100 5650 11100 5900
Wire Wire Line
	10600 6000 10600 6100
Wire Wire Line
	10600 6100 11100 6100
Wire Wire Line
	11100 6100 11100 5900
Connection ~ 11100 5900
Wire Wire Line
	9850 5900 9950 5900
Wire Wire Line
	9850 6000 9950 6000
Text Label 9850 6000 2    50   ~ 0
I2C_SCL
Text Label 9850 5900 2    50   ~ 0
I2C_SDA
Wire Wire Line
	10600 5900 10550 5900
Wire Wire Line
	10650 5800 10600 5800
Wire Wire Line
	10600 5800 10600 5900
Connection ~ 10600 5900
Wire Wire Line
	10600 6000 10550 6000
Wire Wire Line
	10550 6100 10600 6100
Connection ~ 10600 6100
Text Label 9850 6100 2    50   ~ 0
HDC_INT
Wire Wire Line
	9850 6100 9950 6100
Text Label 5450 5850 3    50   ~ 0
HDC_INT
Wire Wire Line
	1900 6200 2200 6200
Connection ~ 1900 6200
Text Label 4600 4100 2    50   ~ 0
OSC_OUT
Text Label 4600 4000 2    50   ~ 0
OSC_IN
Wire Wire Line
	4600 4000 4650 4000
Wire Wire Line
	4600 4100 4650 4100
Wire Wire Line
	6450 4500 6850 4500
Wire Wire Line
	6500 4700 6450 4700
Wire Wire Line
	5550 3150 5550 3200
Text Label 4450 4500 2    50   ~ 0
NRST
Wire Wire Line
	4450 4500 4650 4500
Wire Wire Line
	6450 3900 6550 3900
Wire Wire Line
	5650 3150 5650 3200
Wire Wire Line
	5150 5800 5150 5850
Text HLabel 5050 3150 1    50   Input ~ 0
3V3
Wire Wire Line
	5050 3200 5050 3150
Wire Wire Line
	4450 3600 4700 3600
Wire Wire Line
	4700 3600 4700 3200
Wire Wire Line
	4700 3200 4950 3200
Text HLabel 5850 3150 1    50   Input ~ 0
V_USB
Wire Wire Line
	5850 3200 5850 3150
Text HLabel 4650 3850 1    50   Input ~ 0
3V3
Wire Wire Line
	4650 3900 4650 3850
Text HLabel 4550 4600 0    50   Input ~ 0
3V3
Wire Wire Line
	4550 4600 4650 4600
$Comp
L Device:C C?
U 1 1 60D6BEA4
P 4900 6500
AR Path="/600048E7/60D6BEA4" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60D6BEA4" Ref="C43"  Part="1" 
F 0 "C43" H 5015 6546 50  0000 L CNN
F 1 "15pF 0402" H 5015 6455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 6350 50  0001 C CNN
F 3 "~" H 4900 6500 50  0001 C CNN
	1    4900 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60D6BEAA
P 4900 7100
AR Path="/600048E7/60D6BEAA" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60D6BEAA" Ref="C44"  Part="1" 
F 0 "C44" H 5015 7146 50  0000 L CNN
F 1 "15pF 0402" H 5015 7055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4938 6950 50  0001 C CNN
F 3 "~" H 4900 7100 50  0001 C CNN
	1    4900 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60D6BEB0
P 4650 6750
AR Path="/600048E7/60D6BEB0" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60D6BEB0" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 4650 6500 50  0001 C CNN
F 1 "GND" H 4655 6577 50  0000 C CNN
F 2 "" H 4650 6750 50  0001 C CNN
F 3 "" H 4650 6750 50  0001 C CNN
	1    4650 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 7250 5400 7250
Connection ~ 4900 7250
Wire Wire Line
	4650 6750 4900 6750
Wire Wire Line
	4900 6750 4900 6650
Wire Wire Line
	4900 6750 4900 6950
Connection ~ 4900 6750
Wire Wire Line
	4100 6800 4100 6750
Wire Wire Line
	4100 6750 4650 6750
Connection ~ 4650 6750
$Comp
L power:GND #PWR?
U 1 1 60D6BED0
P 3400 6800
AR Path="/600048E7/60D6BED0" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60D6BED0" Ref="#PWR0150"  Part="1" 
F 0 "#PWR0150" H 3400 6550 50  0001 C CNN
F 1 "GND" H 3405 6627 50  0000 C CNN
F 2 "" H 3400 6800 50  0001 C CNN
F 3 "" H 3400 6800 50  0001 C CNN
	1    3400 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6800 3550 6800
Wire Wire Line
	6450 5000 6450 6800
Wire Wire Line
	5750 5800 5750 5950
Wire Wire Line
	5750 5950 6700 5950
$Comp
L Device:C C?
U 1 1 60DB713D
P 6700 6150
AR Path="/600048E7/60DB713D" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60DB713D" Ref="C49"  Part="1" 
F 0 "C49" H 6815 6196 50  0000 L CNN
F 1 "0.8pF 0402" H 6815 6105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6738 6000 50  0001 C CNN
F 3 "~" H 6700 6150 50  0001 C CNN
	1    6700 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60DC1F09
P 7800 5950
AR Path="/600048E7/60DC1F09" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60DC1F09" Ref="C53"  Part="1" 
F 0 "C53" H 7915 5996 50  0000 L CNN
F 1 "10nF 0402" H 7915 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 5800 50  0001 C CNN
F 3 "~" H 7800 5950 50  0001 C CNN
	1    7800 5950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 60DCCA26
P 8200 6100
AR Path="/600048E7/60DCCA26" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60DCCA26" Ref="C55"  Part="1" 
F 0 "C55" H 8315 6146 50  0000 L CNN
F 1 "1pF 0402" H 8315 6055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8238 5950 50  0001 C CNN
F 3 "~" H 8200 6100 50  0001 C CNN
	1    8200 6100
	-1   0    0    1   
$EndComp
$Comp
L pspice:INDUCTOR L?
U 1 1 60DD88EF
P 8550 5950
AR Path="/600048E7/60DD88EF" Ref="L?"  Part="1" 
AR Path="/6005F2C2/60DD88EF" Ref="L10"  Part="1" 
F 0 "L10" H 8550 6165 50  0000 C CNN
F 1 "3.9nH 0402" H 8550 6074 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 8550 5950 50  0001 C CNN
F 3 "~" H 8550 5950 50  0001 C CNN
	1    8550 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60DE34EA
P 8950 6100
AR Path="/600048E7/60DE34EA" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60DE34EA" Ref="C61"  Part="1" 
F 0 "C61" H 9065 6146 50  0000 L CNN
F 1 "1.2pF 0402" H 9065 6055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8988 5950 50  0001 C CNN
F 3 "~" H 8950 6100 50  0001 C CNN
	1    8950 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	9050 5900 9050 5950
Wire Wire Line
	9050 5950 8950 5950
Wire Wire Line
	8950 5950 8800 5950
Connection ~ 8950 5950
Wire Wire Line
	8300 5950 8200 5950
Wire Wire Line
	7950 5950 8200 5950
Connection ~ 8200 5950
Wire Wire Line
	6700 6000 6700 5950
Connection ~ 6700 5950
Wire Wire Line
	6700 5950 6900 5950
Wire Wire Line
	8200 6250 8850 6250
Wire Wire Line
	8200 6300 8200 6250
Connection ~ 8200 6250
$Comp
L power:GND #PWR?
U 1 1 60E60036
P 8850 6300
AR Path="/60E60036" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60E60036" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60E60036" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 8850 6050 50  0001 C CNN
F 1 "GND" H 8855 6127 50  0000 C CNN
F 2 "" H 8850 6300 50  0001 C CNN
F 3 "" H 8850 6300 50  0001 C CNN
	1    8850 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 6300 8850 6250
Connection ~ 8850 6250
Wire Wire Line
	8850 6250 8950 6250
$Comp
L pspice:CAP C?
U 1 1 60E7DDC6
P 6400 3100
AR Path="/60E7DDC6" Ref="C?"  Part="1" 
AR Path="/600048E7/60E7DDC6" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60E7DDC6" Ref="C47"  Part="1" 
F 0 "C47" H 6578 3146 50  0000 L CNN
F 1 "4.7uF 0402" H 6578 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6400 3100 50  0001 C CNN
F 3 "~" H 6400 3100 50  0001 C CNN
	1    6400 3100
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 60E7DDCC
P 7450 3100
AR Path="/60E7DDCC" Ref="C?"  Part="1" 
AR Path="/600048E7/60E7DDCC" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60E7DDCC" Ref="C51"  Part="1" 
F 0 "C51" H 7628 3146 50  0000 L CNN
F 1 "0.1uF 0402" H 7628 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7450 3100 50  0001 C CNN
F 3 "~" H 7450 3100 50  0001 C CNN
	1    7450 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3350 7150 3350
Wire Wire Line
	6400 2850 6950 2850
Wire Wire Line
	6950 1950 6950 2850
Connection ~ 6950 1950
Wire Wire Line
	6950 1950 6700 1950
Connection ~ 6950 2850
Wire Wire Line
	6950 2850 7450 2850
Wire Wire Line
	6450 4000 7150 4000
Wire Wire Line
	7150 4000 7150 3350
Connection ~ 7150 3350
Wire Wire Line
	7150 3350 7450 3350
Wire Wire Line
	6450 4100 7150 4100
Wire Wire Line
	7150 4100 7150 4000
Connection ~ 7150 4000
$Comp
L pspice:INDUCTOR L?
U 1 1 60EB241B
P 7350 4200
AR Path="/600048E7/60EB241B" Ref="L?"  Part="1" 
AR Path="/6005F2C2/60EB241B" Ref="L9"  Part="1" 
F 0 "L9" V 7304 4278 50  0000 L CNN
F 1 "15uH" V 7395 4278 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 7350 4200 50  0001 C CNN
F 3 "~" H 7350 4200 50  0001 C CNN
	1    7350 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4200 7100 4200
Wire Wire Line
	6450 4400 7600 4400
Wire Wire Line
	7600 4400 7600 4200
Connection ~ 1850 1950
Wire Wire Line
	800  1950 1850 1950
$Comp
L pspice:CAP C?
U 1 1 603969E7
P 800 2200
AR Path="/603969E7" Ref="C?"  Part="1" 
AR Path="/600048E7/603969E7" Ref="C?"  Part="1" 
AR Path="/6005F2C2/603969E7" Ref="C34"  Part="1" 
F 0 "C34" H 978 2246 50  0000 L CNN
F 1 "4.7uF 0402" H 978 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 800 2200 50  0001 C CNN
F 3 "~" H 800 2200 50  0001 C CNN
	1    800  2200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C?
U 1 1 60EE8B97
P 7800 4450
AR Path="/60EE8B97" Ref="C?"  Part="1" 
AR Path="/600048E7/60EE8B97" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60EE8B97" Ref="C52"  Part="1" 
F 0 "C52" H 7978 4496 50  0000 L CNN
F 1 "4.7uF 0402" H 7978 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7800 4450 50  0001 C CNN
F 3 "~" H 7800 4450 50  0001 C CNN
	1    7800 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 4200 7800 4200
Connection ~ 7600 4200
Wire Wire Line
	7800 4700 7600 4700
Wire Wire Line
	7350 4700 7350 4300
Wire Wire Line
	7350 4300 6450 4300
$Comp
L power:GND #PWR?
U 1 1 60F20CD8
P 7600 4750
AR Path="/60F20CD8" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/60F20CD8" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/60F20CD8" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 7600 4500 50  0001 C CNN
F 1 "GND" H 7605 4577 50  0000 C CNN
F 2 "" H 7600 4750 50  0001 C CNN
F 3 "" H 7600 4750 50  0001 C CNN
	1    7600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 4750 7600 4700
Connection ~ 7600 4700
Wire Wire Line
	7600 4700 7350 4700
Text Label 1850 1200 2    50   ~ 0
LED_PA5
Wire Wire Line
	1850 1200 2200 1200
Wire Wire Line
	4950 5800 4950 5850
Wire Wire Line
	6450 4600 6500 4600
Wire Wire Line
	5350 5800 5350 5850
Wire Wire Line
	5450 5800 5450 5850
Text HLabel 5650 5850 3    50   Input ~ 0
3V3
Wire Wire Line
	5650 5800 5650 5850
$Comp
L pspice:CAP C?
U 1 1 610A519D
P 7100 5350
AR Path="/610A519D" Ref="C?"  Part="1" 
AR Path="/600048E7/610A519D" Ref="C?"  Part="1" 
AR Path="/6005F2C2/610A519D" Ref="C50"  Part="1" 
F 0 "C50" H 7278 5396 50  0000 L CNN
F 1 "100pF 0402" H 7278 5305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7100 5350 50  0001 C CNN
F 3 "~" H 7100 5350 50  0001 C CNN
	1    7100 5350
	-1   0    0    1   
$EndComp
$Comp
L pspice:CAP C?
U 1 1 610A51A3
P 7900 5350
AR Path="/610A51A3" Ref="C?"  Part="1" 
AR Path="/600048E7/610A51A3" Ref="C?"  Part="1" 
AR Path="/6005F2C2/610A51A3" Ref="C54"  Part="1" 
F 0 "C54" H 8078 5396 50  0000 L CNN
F 1 "0.1uF 0402" H 8078 5305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7900 5350 50  0001 C CNN
F 3 "~" H 7900 5350 50  0001 C CNN
	1    7900 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	8950 5100 8150 5100
Wire Wire Line
	7100 5100 7900 5100
Connection ~ 7900 5100
Wire Wire Line
	7900 5600 7100 5600
Connection ~ 7900 5600
Wire Wire Line
	8950 5600 8150 5600
$Comp
L pspice:CAP C?
U 1 1 610A51B3
P 8950 5350
AR Path="/610A51B3" Ref="C?"  Part="1" 
AR Path="/600048E7/610A51B3" Ref="C?"  Part="1" 
AR Path="/6005F2C2/610A51B3" Ref="C60"  Part="1" 
F 0 "C60" H 9128 5396 50  0000 L CNN
F 1 "2.2uF 0402" H 9128 5305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8950 5350 50  0001 C CNN
F 3 "~" H 8950 5350 50  0001 C CNN
	1    8950 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 5600 6550 5600
Wire Wire Line
	6550 5600 6550 6100
Connection ~ 7100 5600
Wire Wire Line
	6500 6050 6500 5100
Wire Wire Line
	6500 5100 7100 5100
Connection ~ 7100 5100
Wire Wire Line
	8150 4100 8150 5100
Connection ~ 8150 4100
Connection ~ 8150 5100
Wire Wire Line
	8150 5100 7900 5100
$Comp
L power:GND #PWR?
U 1 1 610E8F20
P 8150 5700
AR Path="/610E8F20" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/610E8F20" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/610E8F20" Ref="#PWR0153"  Part="1" 
F 0 "#PWR0153" H 8150 5450 50  0001 C CNN
F 1 "GND" H 8155 5527 50  0000 C CNN
F 2 "" H 8150 5700 50  0001 C CNN
F 3 "" H 8150 5700 50  0001 C CNN
	1    8150 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5600 8150 5700
Connection ~ 8150 5600
Wire Wire Line
	8150 5600 7900 5600
Connection ~ 4450 3600
$Comp
L pspice:CAP C?
U 1 1 60364B5C
P 4450 3350
AR Path="/60364B5C" Ref="C?"  Part="1" 
AR Path="/600048E7/60364B5C" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60364B5C" Ref="C42"  Part="1" 
F 0 "C42" H 4272 3304 50  0000 R CNN
F 1 "0.1uF 0402" H 4272 3395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4450 3350 50  0001 C CNN
F 3 "~" H 4450 3350 50  0001 C CNN
	1    4450 3350
	-1   0    0    1   
$EndComp
Text HLabel 3850 3650 2    50   Input ~ 0
UART1_TX
Text HLabel 3850 3550 2    50   Input ~ 0
UART1_RX
Wire Wire Line
	3900 4350 3900 4900
Wire Wire Line
	3900 4900 4650 4900
Wire Wire Line
	1750 5000 4650 5000
$Comp
L Device:Antenna_Shield AE2
U 1 1 61207615
P 9050 5700
F 0 "AE2" H 9194 5739 50  0000 L CNN
F 1 "Antenna_Shield" H 9194 5648 50  0000 L CNN
F 2 "RF_Antenna:Texas_SWRA117D_2.4GHz_Left" H 9050 5800 50  0001 C CNN
F 3 "~" H 9050 5800 50  0001 C CNN
	1    9050 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 5900 9150 6250
Wire Wire Line
	9150 6250 8950 6250
Connection ~ 8950 6250
Wire Wire Line
	6700 6300 8200 6300
Wire Wire Line
	950  6200 1250 6200
Wire Wire Line
	1550 6200 1900 6200
$Comp
L Device:Crystal_GND23 Y2
U 1 1 607040FA
P 1400 6200
F 0 "Y2" H 1594 6246 50  0000 L CNN
F 1 "Crystal_GND23" H 1594 6155 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_SeikoEpson_MC146-4Pin_6.7x1.5mm" H 1400 6200 50  0001 C CNN
F 3 "~" H 1400 6200 50  0001 C CNN
	1    1400 6200
	1    0    0    -1  
$EndComp
Text HLabel 6500 4600 2    50   Input ~ 0
DIO3
Wire Wire Line
	2750 3650 3850 3650
Wire Wire Line
	2750 3550 3850 3550
Wire Wire Line
	2950 3100 2950 3250
Wire Wire Line
	2950 3250 2750 3250
Wire Wire Line
	2850 3000 2850 3150
Wire Wire Line
	2850 3150 2750 3150
Wire Wire Line
	2850 3000 3700 3000
Wire Wire Line
	2750 2900 2750 3050
Wire Wire Line
	2750 2900 3550 2900
Wire Wire Line
	5950 5800 5950 6050
Wire Wire Line
	5950 6050 6500 6050
Wire Wire Line
	5850 5800 5850 6100
Wire Wire Line
	5850 6100 6550 6100
$Comp
L Device:Crystal_GND24 Y?
U 1 1 60D6BEB6
P 3700 6800
AR Path="/600048E7/60D6BEB6" Ref="Y?"  Part="1" 
AR Path="/6005F2C2/60D6BEB6" Ref="Y3"  Part="1" 
F 0 "Y3" V 3654 7044 50  0000 L CNN
F 1 "32MHz" V 3745 7044 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 3700 6800 50  0001 C CNN
F 3 "~" H 3700 6800 50  0001 C CNN
	1    3700 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 6600 3700 6350
Wire Wire Line
	3700 6350 4900 6350
Wire Wire Line
	3850 6800 4100 6800
Wire Wire Line
	3700 7000 3700 7250
Wire Wire Line
	3700 7250 4900 7250
Wire Wire Line
	2750 3450 4050 3450
$Comp
L Device:R R?
U 1 1 611CBC8B
P 2600 1000
AR Path="/611CBC8B" Ref="R?"  Part="1" 
AR Path="/600048E7/611CBC8B" Ref="R?"  Part="1" 
AR Path="/6005F2C2/611CBC8B" Ref="R17"  Part="1" 
F 0 "R17" V 2807 1000 50  0000 C CNN
F 1 "4.7K 0402" V 2716 1000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2530 1000 50  0001 C CNN
F 3 "~" H 2600 1000 50  0001 C CNN
	1    2600 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 750  3850 850 
Wire Wire Line
	2200 800  2200 1000
Wire Wire Line
	2200 1000 2450 1000
$Comp
L Device:LED D3
U 1 1 611CBC94
P 3250 1000
F 0 "D3" H 3243 1216 50  0000 C CNN
F 1 "LED" H 3243 1125 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3250 1000 50  0001 C CNN
F 3 "~" H 3250 1000 50  0001 C CNN
	1    3250 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1000 3400 850 
Wire Wire Line
	3400 850  3850 850 
Wire Wire Line
	2750 1000 3100 1000
Text Label 1850 800  2    50   ~ 0
LED_PA4
Wire Wire Line
	1850 800  2200 800 
$Comp
L Device:R R?
U 1 1 611DB580
P 5100 1000
AR Path="/611DB580" Ref="R?"  Part="1" 
AR Path="/600048E7/611DB580" Ref="R?"  Part="1" 
AR Path="/6005F2C2/611DB580" Ref="R21"  Part="1" 
F 0 "R21" V 5307 1000 50  0000 C CNN
F 1 "4.7K 0402" V 5216 1000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5030 1000 50  0001 C CNN
F 3 "~" H 5100 1000 50  0001 C CNN
	1    5100 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 750  6350 850 
Wire Wire Line
	4700 800  4700 1000
Wire Wire Line
	4700 1000 4950 1000
$Comp
L Device:LED D6
U 1 1 611DB589
P 5750 1000
F 0 "D6" H 5743 1216 50  0000 C CNN
F 1 "LED" H 5743 1125 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5750 1000 50  0001 C CNN
F 3 "~" H 5750 1000 50  0001 C CNN
	1    5750 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1000 5900 850 
Wire Wire Line
	5900 850  6350 850 
Wire Wire Line
	5250 1000 5600 1000
Text Label 4350 800  2    50   ~ 0
LED_PA12
Wire Wire Line
	4350 800  4700 800 
$Comp
L Device:R R?
U 1 1 611EBD5E
P 5150 1450
AR Path="/611EBD5E" Ref="R?"  Part="1" 
AR Path="/600048E7/611EBD5E" Ref="R?"  Part="1" 
AR Path="/6005F2C2/611EBD5E" Ref="R22"  Part="1" 
F 0 "R22" V 5357 1450 50  0000 C CNN
F 1 "4.7K 0402" V 5266 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5080 1450 50  0001 C CNN
F 3 "~" H 5150 1450 50  0001 C CNN
	1    5150 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 1200 6400 1300
Wire Wire Line
	4750 1250 4750 1450
Wire Wire Line
	4750 1450 5000 1450
$Comp
L Device:LED D7
U 1 1 611EBD67
P 5800 1450
F 0 "D7" H 5793 1666 50  0000 C CNN
F 1 "LED" H 5793 1575 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5800 1450 50  0001 C CNN
F 3 "~" H 5800 1450 50  0001 C CNN
	1    5800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1450 5950 1300
Wire Wire Line
	5950 1300 6400 1300
Wire Wire Line
	5300 1450 5650 1450
Text Label 4400 1250 2    50   ~ 0
LED_PA7
Wire Wire Line
	4400 1250 4750 1250
$Comp
L Device:R R?
U 1 1 611FD627
P 4000 1700
AR Path="/611FD627" Ref="R?"  Part="1" 
AR Path="/600048E7/611FD627" Ref="R?"  Part="1" 
AR Path="/6005F2C2/611FD627" Ref="R19"  Part="1" 
F 0 "R19" V 4207 1700 50  0000 C CNN
F 1 "4.7K 0402" V 4116 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 1700 50  0001 C CNN
F 3 "~" H 4000 1700 50  0001 C CNN
	1    4000 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 1600 5250 1700
Wire Wire Line
	3600 1650 3600 1700
Wire Wire Line
	3600 1700 3850 1700
$Comp
L Device:LED D5
U 1 1 611FD630
P 4650 1700
F 0 "D5" H 4643 1916 50  0000 C CNN
F 1 "LED" H 4643 1825 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4650 1700 50  0001 C CNN
F 3 "~" H 4650 1700 50  0001 C CNN
	1    4650 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1700 5250 1700
Wire Wire Line
	4150 1700 4500 1700
Text Label 3250 1650 2    50   ~ 0
LED_PA8
Wire Wire Line
	3250 1650 3600 1650
$Comp
L Device:R R?
U 1 1 6125B561
P 9600 2850
AR Path="/6125B561" Ref="R?"  Part="1" 
AR Path="/600048E7/6125B561" Ref="R?"  Part="1" 
AR Path="/6005F2C2/6125B561" Ref="R27"  Part="1" 
F 0 "R27" V 9807 2850 50  0000 C CNN
F 1 "4.7K 0402" V 9716 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9530 2850 50  0001 C CNN
F 3 "~" H 9600 2850 50  0001 C CNN
	1    9600 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 2600 10850 2700
$Comp
L Device:LED D8
U 1 1 6125B56A
P 10250 2850
F 0 "D8" H 10243 3066 50  0000 C CNN
F 1 "LED" H 10243 2975 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 10250 2850 50  0001 C CNN
F 3 "~" H 10250 2850 50  0001 C CNN
	1    10250 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 2850 10400 2700
Wire Wire Line
	10400 2700 10850 2700
Wire Wire Line
	9750 2850 10100 2850
Wire Wire Line
	8800 2850 9450 2850
$Comp
L pspice:INDUCTOR L?
U 1 1 612FB9DD
P 7150 5950
AR Path="/600048E7/612FB9DD" Ref="L?"  Part="1" 
AR Path="/6005F2C2/612FB9DD" Ref="L8"  Part="1" 
F 0 "L8" H 7150 6165 50  0000 C CNN
F 1 "3.9nH 0402" H 7150 6074 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 7150 5950 50  0001 C CNN
F 3 "~" H 7150 5950 50  0001 C CNN
	1    7150 5950
	1    0    0    -1  
$EndComp
$Comp
L STM32WB55RGV6:TL3315NF100Q S?
U 1 1 60081584
P 7950 1250
AR Path="/60081584" Ref="S?"  Part="1" 
AR Path="/600048E7/60081584" Ref="S?"  Part="1" 
AR Path="/6005F2C2/60081584" Ref="S2"  Part="1" 
F 0 "S2" H 7950 1526 50  0000 C CNN
F 1 "TL3315NF100Q" H 7950 1250 50  0001 L BNN
F 2 "TL3315NF100Q" H 7950 1250 50  0001 L BNN
F 3 "EG4620CT-ND" H 7950 1250 50  0001 L BNN
	1    7950 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5950 7650 5950
Text HLabel 7150 3600 0    50   Input ~ 0
3V3
Wire Wire Line
	2750 3350 4050 3350
Text HLabel 5750 7150 0    50   Input ~ 0
3V3E
Text HLabel 10650 5800 1    50   Input ~ 0
3V3E
Text HLabel 10850 2600 2    50   Input ~ 0
3V3E
Text HLabel 5250 1600 2    50   Input ~ 0
3V3E
Text HLabel 6400 1200 2    50   Input ~ 0
3V3E
Text HLabel 6350 750  2    50   Input ~ 0
3V3E
Text HLabel 3850 750  2    50   Input ~ 0
3V3E
Text HLabel 3850 1150 2    50   Input ~ 0
3V3E
Text Label 6300 2550 0    50   ~ 0
LED_PA12
Text Label 6300 2650 0    50   ~ 0
LED_PA11
Text Label 8800 2850 2    50   ~ 0
LED_PA11
Wire Wire Line
	6350 2650 6150 2650
Wire Wire Line
	6150 2650 6150 3200
Wire Wire Line
	6300 2550 6050 2550
Wire Wire Line
	6050 2550 6050 3200
Wire Wire Line
	5750 3150 5750 3200
Wire Wire Line
	5950 3150 5950 3200
Text Label 4050 3350 0    50   ~ 0
LED_PA13
$Comp
L Device:R R?
U 1 1 61223B1E
P 9600 3200
AR Path="/61223B1E" Ref="R?"  Part="1" 
AR Path="/600048E7/61223B1E" Ref="R?"  Part="1" 
AR Path="/6005F2C2/61223B1E" Ref="R5"  Part="1" 
F 0 "R5" V 9807 3200 50  0000 C CNN
F 1 "4.7K 0402" V 9716 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9530 3200 50  0001 C CNN
F 3 "~" H 9600 3200 50  0001 C CNN
	1    9600 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 61223B24
P 10250 3200
F 0 "D1" H 10243 3416 50  0000 C CNN
F 1 "LED" H 10243 3325 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 10250 3200 50  0001 C CNN
F 3 "~" H 10250 3200 50  0001 C CNN
	1    10250 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 3200 10100 3200
Wire Wire Line
	8800 3200 9450 3200
Text Label 8800 3200 2    50   ~ 0
LED_PA14
Text Label 4050 3450 0    50   ~ 0
LED_PA14
$Comp
L Device:R R?
U 1 1 6124B2E1
P 4700 3000
AR Path="/6124B2E1" Ref="R?"  Part="1" 
AR Path="/600048E7/6124B2E1" Ref="R?"  Part="1" 
AR Path="/6005F2C2/6124B2E1" Ref="R4"  Part="1" 
F 0 "R4" V 4907 3000 50  0000 C CNN
F 1 "100K 0402" V 4816 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4630 3000 50  0001 C CNN
F 3 "~" H 4700 3000 50  0001 C CNN
	1    4700 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 3000 4450 3000
Wire Wire Line
	4450 3000 4450 3100
Connection ~ 4450 3100
Wire Wire Line
	4850 3000 4850 3150
Wire Wire Line
	4850 3150 5050 3150
Wire Wire Line
	4900 6350 5800 6350
Wire Wire Line
	5800 6350 5800 6800
Wire Wire Line
	5800 6800 6450 6800
Connection ~ 4900 6350
Wire Wire Line
	5400 6500 6050 6500
Wire Wire Line
	5400 6500 5400 7250
Wire Wire Line
	6050 5800 6050 6500
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 612CEF0D
P 2550 3050
F 0 "J1" H 2578 3076 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 2985 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3050 50  0001 C CNN
F 3 "~" H 2550 3050 50  0001 C CNN
	1    2550 3050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 612E474F
P 2550 3150
F 0 "J2" H 2578 3176 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3085 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3150 50  0001 C CNN
F 3 "~" H 2550 3150 50  0001 C CNN
	1    2550 3150
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 612F8EAC
P 2550 3250
F 0 "J3" H 2578 3276 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3185 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3250 50  0001 C CNN
F 3 "~" H 2550 3250 50  0001 C CNN
	1    2550 3250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 6130D4BB
P 2550 3350
F 0 "J4" H 2578 3376 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3285 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3350 50  0001 C CNN
F 3 "~" H 2550 3350 50  0001 C CNN
	1    2550 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 61321A70
P 2550 3450
F 0 "J5" H 2578 3476 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3385 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3450 50  0001 C CNN
F 3 "~" H 2550 3450 50  0001 C CNN
	1    2550 3450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 61336050
P 2550 3550
F 0 "J6" H 2578 3576 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3485 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3550 50  0001 C CNN
F 3 "~" H 2550 3550 50  0001 C CNN
	1    2550 3550
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 6134A717
P 2550 3650
F 0 "J7" H 2578 3676 50  0000 L CNN
F 1 "Conn_01x01_Female" H 2578 3585 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2550 3650 50  0001 C CNN
F 3 "~" H 2550 3650 50  0001 C CNN
	1    2550 3650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV2
U 1 1 613DC2FF
P 6300 7350
F 0 "RV2" V 6195 7350 50  0000 C CNN
F 1 "R_POT_Small" V 6104 7350 50  0000 C CNN
F 2 "stm32wb_lora_EU_card444:TRIM_TC33X-2-104E" H 6300 7350 50  0001 C CNN
F 3 "~" H 6300 7350 50  0001 C CNN
	1    6300 7350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 613DC305
P 6050 7350
AR Path="/600048E7/613DC305" Ref="#PWR?"  Part="1" 
AR Path="/6005F2C2/613DC305" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 6050 7100 50  0001 C CNN
F 1 "GND" H 6055 7177 50  0000 C CNN
F 2 "" H 6050 7350 50  0001 C CNN
F 3 "" H 6050 7350 50  0001 C CNN
	1    6050 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 7350 6200 7350
Wire Wire Line
	6400 7350 6450 7350
Wire Wire Line
	6450 7000 6450 7350
Wire Wire Line
	10400 2850 10400 3200
Connection ~ 10400 2850
Text HLabel 2200 800  2    50   Input ~ 0
GPS_1PPS
Text HLabel 2200 1200 2    50   Input ~ 0
GPS_TIMER
Text HLabel 3600 1450 2    50   Input ~ 0
AADET_N
Wire Wire Line
	3600 1450 3600 1650
Connection ~ 3600 1650
Text HLabel 3850 2950 2    50   Input ~ 0
GPS_RESET
Wire Wire Line
	3850 2950 3850 3100
Wire Wire Line
	2950 3100 3850 3100
Connection ~ 3850 3100
Wire Wire Line
	3850 3100 4450 3100
Text Label 2200 6200 0    50   ~ 0
OSC_OUT
Text Label 2200 6300 0    50   ~ 0
OSC_IN
Wire Wire Line
	6450 7000 6200 7000
Wire Wire Line
	6200 7000 6200 7150
Wire Wire Line
	6200 7150 5750 7150
Wire Wire Line
	6300 7250 6300 6250
Text Label 4600 4800 2    50   ~ 0
ADC1
Wire Wire Line
	4300 5100 4600 5100
Wire Wire Line
	4600 5100 4600 4800
Wire Wire Line
	4300 6250 6300 6250
Wire Wire Line
	4300 5100 4300 6250
Text HLabel 5550 5850 3    50   Input ~ 0
HORNEN
Wire Wire Line
	5550 5800 5550 5850
Text HLabel 4950 5850 3    50   Input ~ 0
EXT_SW
Text Label 4850 5850 2    50   ~ 0
LED_PA4
Text Label 5250 5900 3    50   ~ 0
LED_PA7
Text Label 5050 5850 3    50   ~ 0
LED_PA5
Text HLabel 5150 5850 3    50   Input ~ 0
3V3E_EN
Wire Wire Line
	4850 5850 4950 5850
Wire Wire Line
	4950 5800 4850 5800
Wire Wire Line
	4850 5800 4850 5850
Connection ~ 4950 5800
Text HLabel 4750 6200 0    50   Input ~ 0
ADS_OUT
Text HLabel 4750 6100 0    50   Input ~ 0
ADS_SCK
Wire Wire Line
	4750 6100 5050 6100
Wire Wire Line
	5050 5800 5050 6100
Wire Wire Line
	4750 6200 5250 6200
Wire Wire Line
	5250 5800 5250 6200
$EndSCHEMATC
