EESchema Schematic File Version 4
LIBS:L80RE_Lora_murata-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L80RE_Lora_murata-rescue:USB3075-30-A-stm32wb_lora_EU_card-rescue J2
U 1 1 60189F26
P 1100 5850
F 0 "J2" H 1043 5283 50  0000 C CNN
F 1 "USB3075-30-A" H 1043 5374 50  0000 C CNN
F 2 "GCT_USB3075-30-A" H 1100 5850 50  0001 L BNN
F 3 "" H 1100 5850 50  0001 L BNN
F 4 "2.66mm" H 1100 5850 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 5 "Global Connector Technology" H 1100 5850 50  0001 L BNN "MANUFACTURER"
F 6 "D3" H 1100 5850 50  0001 L BNN "PARTREV"
F 7 "Manufacturer recommendations" H 1100 5850 50  0001 L BNN "STANDARD"
	1    1100 5850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 601910D9
P 1600 5500
F 0 "#PWR0141" H 1600 5250 50  0001 C CNN
F 1 "GND" H 1605 5327 50  0000 C CNN
F 2 "" H 1600 5500 50  0001 C CNN
F 3 "" H 1600 5500 50  0001 C CNN
	1    1600 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 6250 1650 6150
Text HLabel 1500 5950 2    50   Input ~ 0
USB_DP
Text HLabel 1500 6050 2    50   Input ~ 0
USB_DM
Wire Wire Line
	1500 6150 1650 6150
Wire Wire Line
	1600 5500 1500 5500
Wire Wire Line
	1500 5500 1500 5550
Wire Wire Line
	1500 5750 1500 5550
Connection ~ 1500 5550
$Comp
L power:GND #PWR0142
U 1 1 60199102
P 7600 1350
F 0 "#PWR0142" H 7600 1100 50  0001 C CNN
F 1 "GND" H 7605 1177 50  0000 C CNN
F 2 "" H 7600 1350 50  0001 C CNN
F 3 "" H 7600 1350 50  0001 C CNN
	1    7600 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 1350 7750 1350
$Comp
L power:GND #PWR0144
U 1 1 601AC038
P 9150 6200
F 0 "#PWR0144" H 9150 5950 50  0001 C CNN
F 1 "GND" H 9155 6027 50  0000 C CNN
F 2 "" H 9150 6200 50  0001 C CNN
F 3 "" H 9150 6200 50  0001 C CNN
	1    9150 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 601B0FF2
P 10050 5350
AR Path="/601B0FF2" Ref="R?"  Part="1" 
AR Path="/600048E7/601B0FF2" Ref="R?"  Part="1" 
AR Path="/6005F2C2/601B0FF2" Ref="R?"  Part="1" 
AR Path="/60188217/601B0FF2" Ref="R16"  Part="1" 
F 0 "R16" V 10257 5350 50  0000 C CNN
F 1 "510K 0402" V 10166 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9980 5350 50  0001 C CNN
F 3 "~" H 10050 5350 50  0001 C CNN
	1    10050 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	10050 5700 10050 5600
$Comp
L Device:R R?
U 1 1 601B16B4
P 10050 5850
AR Path="/601B16B4" Ref="R?"  Part="1" 
AR Path="/600048E7/601B16B4" Ref="R?"  Part="1" 
AR Path="/6005F2C2/601B16B4" Ref="R?"  Part="1" 
AR Path="/60188217/601B16B4" Ref="R17"  Part="1" 
F 0 "R17" V 10257 5850 50  0000 C CNN
F 1 "100K 0402" V 10166 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9980 5850 50  0001 C CNN
F 3 "~" H 10050 5850 50  0001 C CNN
	1    10050 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	10050 6200 10050 6000
Wire Wire Line
	10050 6200 9150 6200
Connection ~ 10050 5600
Wire Wire Line
	10050 5600 10050 5500
Wire Wire Line
	10050 4950 10050 5200
$Comp
L pspice:CAP C?
U 1 1 601B37E5
P 10900 5400
AR Path="/601B37E5" Ref="C?"  Part="1" 
AR Path="/600048E7/601B37E5" Ref="C?"  Part="1" 
AR Path="/6005F2C2/601B37E5" Ref="C?"  Part="1" 
AR Path="/60188217/601B37E5" Ref="C35"  Part="1" 
F 0 "C35" H 11078 5446 50  0000 L CNN
F 1 "10uF 0603" H 11078 5355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10900 5400 50  0001 C CNN
F 3 "~" H 10900 5400 50  0001 C CNN
	1    10900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 4950 10900 5150
Wire Wire Line
	10900 5650 10900 6200
Wire Wire Line
	7250 5850 7250 5200
Wire Wire Line
	7250 5200 7400 5200
Text HLabel 1650 6250 2    50   Input ~ 0
V_USB
Text HLabel 10900 4950 2    50   Input ~ 0
3V3U
$Comp
L pspice:CAP C?
U 1 1 601C462C
P 4700 6400
AR Path="/601C462C" Ref="C?"  Part="1" 
AR Path="/600048E7/601C462C" Ref="C?"  Part="1" 
AR Path="/6005F2C2/601C462C" Ref="C?"  Part="1" 
AR Path="/60188217/601C462C" Ref="C34"  Part="1" 
F 0 "C34" H 4878 6446 50  0000 L CNN
F 1 "10uF 0603" H 4878 6355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4700 6400 50  0001 C CNN
F 3 "~" H 4700 6400 50  0001 C CNN
	1    4700 6400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 6150 4700 5600
$Comp
L power:GND #PWR0145
U 1 1 601C68C4
P 4700 6700
F 0 "#PWR0145" H 4700 6450 50  0001 C CNN
F 1 "GND" H 4705 6527 50  0000 C CNN
F 2 "" H 4700 6700 50  0001 C CNN
F 3 "" H 4700 6700 50  0001 C CNN
	1    4700 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 6700 4700 6650
$Comp
L Transistor_FET:IRF9383M Q2
U 1 1 601FA171
P 2850 3200
F 0 "Q2" V 3193 3200 50  0000 C CNN
F 1 "IRF9383M" V 3102 3200 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2850 3200 50  0001 C CIN
F 3 "https://www.infineon.com/dgdl/irf9383mpbf.pdf?fileId=5546d462533600a40153561169a11dab" H 2850 3200 50  0001 L CNN
	1    2850 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 601FE177
P 4000 3050
AR Path="/601FE177" Ref="R?"  Part="1" 
AR Path="/600048E7/601FE177" Ref="R?"  Part="1" 
AR Path="/6005F2C2/601FE177" Ref="R?"  Part="1" 
AR Path="/60188217/601FE177" Ref="R19"  Part="1" 
F 0 "R19" V 4207 3050 50  0000 C CNN
F 1 "10M 0402" V 4116 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3050 50  0001 C CNN
F 3 "~" H 4000 3050 50  0001 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7000 Q3
U 1 1 60200012
P 4650 3400
F 0 "Q3" H 4856 3354 50  0000 L CNN
F 1 "2N7000" H 4856 3445 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4850 3325 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 4650 3400 50  0001 L CNN
	1    4650 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 602046BF
P 3150 3750
AR Path="/602046BF" Ref="R?"  Part="1" 
AR Path="/600048E7/602046BF" Ref="R?"  Part="1" 
AR Path="/6005F2C2/602046BF" Ref="R?"  Part="1" 
AR Path="/60188217/602046BF" Ref="R18"  Part="1" 
F 0 "R18" V 3357 3750 50  0000 C CNN
F 1 "1K 0402" V 3266 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3080 3750 50  0001 C CNN
F 3 "~" H 3150 3750 50  0001 C CNN
	1    3150 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60206ECC
P 5150 3400
AR Path="/60206ECC" Ref="R?"  Part="1" 
AR Path="/600048E7/60206ECC" Ref="R?"  Part="1" 
AR Path="/6005F2C2/60206ECC" Ref="R?"  Part="1" 
AR Path="/60188217/60206ECC" Ref="R20"  Part="1" 
F 0 "R20" V 5357 3400 50  0000 C CNN
F 1 "1K 0402" V 5266 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5080 3400 50  0001 C CNN
F 3 "~" H 5150 3400 50  0001 C CNN
	1    5150 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 3400 5650 3400
$Comp
L Device:R R?
U 1 1 60208341
P 5650 3550
AR Path="/60208341" Ref="R?"  Part="1" 
AR Path="/600048E7/60208341" Ref="R?"  Part="1" 
AR Path="/6005F2C2/60208341" Ref="R?"  Part="1" 
AR Path="/60188217/60208341" Ref="R21"  Part="1" 
F 0 "R21" V 5857 3550 50  0000 C CNN
F 1 "10M 0402" V 5766 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5580 3550 50  0001 C CNN
F 3 "~" H 5650 3550 50  0001 C CNN
	1    5650 3550
	1    0    0    -1  
$EndComp
Text HLabel 5950 3400 2    50   Input ~ 0
3V3E_EN
Wire Wire Line
	5650 3400 5950 3400
Connection ~ 5650 3400
Wire Wire Line
	5000 3400 4850 3400
$Comp
L power:GND #PWR0148
U 1 1 6020D2B8
P 4350 3150
F 0 "#PWR0148" H 4350 2900 50  0001 C CNN
F 1 "GND" H 4355 2977 50  0000 C CNN
F 2 "" H 4350 3150 50  0001 C CNN
F 3 "" H 4350 3150 50  0001 C CNN
	1    4350 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 3150 4550 3150
Wire Wire Line
	4550 3150 4550 3200
$Comp
L power:GND #PWR0149
U 1 1 60210500
P 5650 3850
F 0 "#PWR0149" H 5650 3600 50  0001 C CNN
F 1 "GND" H 5655 3677 50  0000 C CNN
F 2 "" H 5650 3850 50  0001 C CNN
F 3 "" H 5650 3850 50  0001 C CNN
	1    5650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3700 5650 3850
Wire Wire Line
	4550 3600 4550 3750
Wire Wire Line
	3300 3750 3750 3750
Wire Wire Line
	3000 3750 2850 3750
Wire Wire Line
	2850 3750 2850 3400
$Comp
L pspice:CAP C?
U 1 1 6021F564
P 3750 3150
AR Path="/6021F564" Ref="C?"  Part="1" 
AR Path="/600048E7/6021F564" Ref="C?"  Part="1" 
AR Path="/6005F2C2/6021F564" Ref="C?"  Part="1" 
AR Path="/60188217/6021F564" Ref="C44"  Part="1" 
F 0 "C44" H 3928 3196 50  0000 L CNN
F 1 "10pF 0402" H 3928 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3750 3150 50  0001 C CNN
F 3 "~" H 3750 3150 50  0001 C CNN
	1    3750 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3400 3750 3750
Connection ~ 3750 3750
Wire Wire Line
	3750 3750 4000 3750
Wire Wire Line
	4000 3200 4000 3750
Connection ~ 4000 3750
Wire Wire Line
	4000 3750 4550 3750
Wire Wire Line
	4000 2900 3750 2900
Wire Wire Line
	3750 2900 3350 2900
Wire Wire Line
	3050 2900 3050 3100
Connection ~ 3750 2900
Text HLabel 2550 3100 0    50   Input ~ 0
3V3E
Wire Wire Line
	2550 3100 2650 3100
Text HLabel 3350 2850 1    50   Input ~ 0
3V3
Wire Wire Line
	3350 2850 3350 2900
Connection ~ 3350 2900
Wire Wire Line
	3350 2900 3050 2900
$Comp
L L80RE_Lora_murata-rescue:SS312SAH4-R-stm32wb_lora_EU_card-rescue S2
U 1 1 6136E81B
P 7800 5100
F 0 "S2" H 7800 5467 50  0000 C CNN
F 1 "SS312SAH4-R" H 7800 5376 50  0000 C CNN
F 2 "SW_SS312SAH4-R" H 7800 5100 50  0001 L BNN
F 3 "" H 7800 5100 50  0001 L BNN
F 4 "NKK switches" H 7800 5100 50  0001 L BNN "MANUFACTURER"
	1    7800 5100
	-1   0    0    1   
$EndComp
$Comp
L hx711:MIC5205-3.3YM5 U5
U 1 1 60EFCC94
P 9150 5050
F 0 "U5" H 9150 5392 50  0000 C CNN
F 1 "MIC5205-3.3YM5" H 9150 5301 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 9150 5375 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 9150 5050 50  0001 C CNN
	1    9150 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5100 8350 5100
Wire Wire Line
	8350 5100 8350 4950
Wire Wire Line
	8350 4950 8850 4950
$Comp
L pspice:INDUCTOR L?
U 1 1 60F04533
P 9800 4950
AR Path="/600048E7/60F04533" Ref="L?"  Part="1" 
AR Path="/6005F2C2/60F04533" Ref="L?"  Part="1" 
AR Path="/60188217/60F04533" Ref="L9"  Part="1" 
F 0 "L9" V 9754 5028 50  0000 L CNN
F 1 "2.2uH" V 9845 5028 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 9800 4950 50  0001 C CNN
F 3 "~" H 9800 4950 50  0001 C CNN
	1    9800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4950 9500 4950
Wire Wire Line
	9500 5600 10050 5600
$Comp
L pspice:CAP C?
U 1 1 60F0AF50
P 10350 5250
AR Path="/60F0AF50" Ref="C?"  Part="1" 
AR Path="/600048E7/60F0AF50" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60F0AF50" Ref="C?"  Part="1" 
AR Path="/60188217/60F0AF50" Ref="C62"  Part="1" 
F 0 "C62" H 10528 5296 50  0000 L CNN
F 1 "22pF 0402" H 10528 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10350 5250 50  0001 C CNN
F 3 "~" H 10350 5250 50  0001 C CNN
	1    10350 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 4950 10200 4950
Connection ~ 10050 4950
Wire Wire Line
	10350 5000 10350 4950
Connection ~ 10350 4950
Wire Wire Line
	10050 5600 10350 5600
Wire Wire Line
	10350 5600 10350 5500
Wire Wire Line
	10050 6200 10900 6200
Connection ~ 10050 6200
Wire Wire Line
	9150 5350 9150 5850
Connection ~ 9150 6200
Wire Wire Line
	7250 5850 9150 5850
Connection ~ 9150 5850
Wire Wire Line
	9150 5850 9150 6200
Wire Wire Line
	9500 4950 9500 5600
Wire Wire Line
	9450 5050 9450 5600
Wire Wire Line
	2900 5600 2900 6150
Wire Wire Line
	2900 6150 1650 6150
Connection ~ 1650 6150
Wire Wire Line
	8850 5050 8650 5050
Wire Wire Line
	8650 5050 8650 4650
Wire Wire Line
	8650 4650 9550 4650
Wire Wire Line
	9550 4650 9550 4950
Wire Wire Line
	5050 5000 5050 5600
Connection ~ 5050 5600
Wire Wire Line
	5050 5600 4700 5600
Text HLabel 10900 2100 2    50   Input ~ 0
3V3
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 60F2156E
P 8050 2250
F 0 "J7" H 8078 2276 50  0000 L CNN
F 1 "Conn_01x03_Female" H 8078 2185 50  0000 L CNN
F 2 "stm32wb_lora_EU_card444:CR123A_holder" H 8050 2250 50  0001 C CNN
F 3 "~" H 8050 2250 50  0001 C CNN
	1    8050 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Female J6
U 1 1 60F23E7D
P 7950 1150
F 0 "J6" H 7978 1176 50  0000 L CNN
F 1 "Conn_01x03_Female" H 7978 1085 50  0000 L CNN
F 2 "stm32wb_lora_EU_card444:CR123A_holder" H 7950 1150 50  0001 C CNN
F 3 "~" H 7950 1150 50  0001 C CNN
	1    7950 1150
	1    0    0    -1  
$EndComp
Connection ~ 4700 5600
Wire Wire Line
	4700 5600 2900 5600
Wire Wire Line
	7750 1050 7750 1150
Wire Wire Line
	7750 1250 7750 1150
Connection ~ 7750 1150
Wire Wire Line
	7750 1350 7750 1250
Connection ~ 7750 1250
Wire Wire Line
	8250 2350 8250 2250
Wire Wire Line
	8250 2250 8250 2150
Connection ~ 8250 2250
$Comp
L Device:R R?
U 1 1 60F3A826
P 1950 5600
AR Path="/60F3A826" Ref="R?"  Part="1" 
AR Path="/600048E7/60F3A826" Ref="R?"  Part="1" 
AR Path="/6005F2C2/60F3A826" Ref="R?"  Part="1" 
AR Path="/60188217/60F3A826" Ref="R22"  Part="1" 
F 0 "R22" V 2157 5600 50  0000 C CNN
F 1 "100K 0402" V 2066 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1880 5600 50  0001 C CNN
F 3 "~" H 1950 5600 50  0001 C CNN
	1    1950 5600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 60F3A82C
P 1950 5300
F 0 "#PWR0150" H 1950 5050 50  0001 C CNN
F 1 "GND" H 1955 5127 50  0000 C CNN
F 2 "" H 1950 5300 50  0001 C CNN
F 3 "" H 1950 5300 50  0001 C CNN
	1    1950 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 5450 1950 5300
Wire Wire Line
	1500 5850 1950 5850
Wire Wire Line
	1950 5850 1950 5750
$Comp
L pspice:CAP C?
U 1 1 60F3ED48
P 5550 6400
AR Path="/60F3ED48" Ref="C?"  Part="1" 
AR Path="/600048E7/60F3ED48" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60F3ED48" Ref="C?"  Part="1" 
AR Path="/60188217/60F3ED48" Ref="C60"  Part="1" 
F 0 "C60" H 5728 6446 50  0000 L CNN
F 1 "10uF 0603" H 5728 6355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5550 6400 50  0001 C CNN
F 3 "~" H 5550 6400 50  0001 C CNN
	1    5550 6400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0151
U 1 1 60F3ED4E
P 5550 6700
F 0 "#PWR0151" H 5550 6450 50  0001 C CNN
F 1 "GND" H 5555 6527 50  0000 C CNN
F 2 "" H 5550 6700 50  0001 C CNN
F 3 "" H 5550 6700 50  0001 C CNN
	1    5550 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 6700 5550 6650
Wire Wire Line
	5050 5600 5550 5600
Connection ~ 5550 5600
Wire Wire Line
	5550 5600 9450 5600
Wire Wire Line
	5550 5600 5550 6150
Wire Wire Line
	5050 5000 7400 5000
$Comp
L Diode:1N5819 D2
U 1 1 60F4A880
P 10200 4800
F 0 "D2" V 10154 4879 50  0000 L CNN
F 1 "1N5819" V 10245 4879 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 10200 4625 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 10200 4800 50  0001 C CNN
	1    10200 4800
	0    1    1    0   
$EndComp
$Comp
L pspice:CAP C?
U 1 1 60F4DDC8
P 9150 2900
AR Path="/60F4DDC8" Ref="C?"  Part="1" 
AR Path="/600048E7/60F4DDC8" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60F4DDC8" Ref="C?"  Part="1" 
AR Path="/60188217/60F4DDC8" Ref="C61"  Part="1" 
F 0 "C61" H 9328 2946 50  0000 L CNN
F 1 "10uF 0603" H 9328 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9150 2900 50  0001 C CNN
F 3 "~" H 9150 2900 50  0001 C CNN
	1    9150 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	9150 2650 9150 2100
$Comp
L power:GND #PWR0152
U 1 1 60F4DDCF
P 9150 3200
F 0 "#PWR0152" H 9150 2950 50  0001 C CNN
F 1 "GND" H 9155 3027 50  0000 C CNN
F 2 "" H 9150 3200 50  0001 C CNN
F 3 "" H 9150 3200 50  0001 C CNN
	1    9150 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3200 9150 3150
$Comp
L pspice:CAP C?
U 1 1 60F4DDD6
P 10900 2900
AR Path="/60F4DDD6" Ref="C?"  Part="1" 
AR Path="/600048E7/60F4DDD6" Ref="C?"  Part="1" 
AR Path="/6005F2C2/60F4DDD6" Ref="C?"  Part="1" 
AR Path="/60188217/60F4DDD6" Ref="C63"  Part="1" 
F 0 "C63" H 11078 2946 50  0000 L CNN
F 1 "10uF 0603" H 11078 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10900 2900 50  0001 C CNN
F 3 "~" H 10900 2900 50  0001 C CNN
	1    10900 2900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0153
U 1 1 60F4DDDC
P 10900 3200
F 0 "#PWR0153" H 10900 2950 50  0001 C CNN
F 1 "GND" H 10905 3027 50  0000 C CNN
F 2 "" H 10900 3200 50  0001 C CNN
F 3 "" H 10900 3200 50  0001 C CNN
	1    10900 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 3200 10900 3150
Wire Wire Line
	10900 2100 10900 2650
Connection ~ 10200 4950
Wire Wire Line
	10200 4950 10350 4950
Wire Wire Line
	10350 4950 10900 4950
Wire Wire Line
	10900 2100 10200 2100
Wire Wire Line
	10200 2100 10200 4650
Connection ~ 10200 2100
Wire Wire Line
	8250 2100 8250 2150
Wire Wire Line
	8250 2100 8350 2100
Connection ~ 9150 2100
Wire Wire Line
	9150 2100 10200 2100
Connection ~ 8250 2150
$Comp
L Device:Battery_Cell BT1
U 1 1 615D4917
P 8350 1700
F 0 "BT1" H 8232 1704 50  0000 R CNN
F 1 "Battery_Cell" H 8232 1795 50  0000 R CNN
F 2 "Battery:BatteryHolder_Keystone_3009_1x2450" V 8350 1760 50  0001 C CNN
F 3 "~" V 8350 1760 50  0001 C CNN
	1    8350 1700
	-1   0    0    1   
$EndComp
Wire Wire Line
	8350 1900 8350 2100
Connection ~ 8350 2100
Wire Wire Line
	8350 2100 9150 2100
Wire Wire Line
	8350 1600 8350 1350
Wire Wire Line
	8350 1350 7750 1350
Connection ~ 7750 1350
$EndSCHEMATC
