EESchema Schematic File Version 4
LIBS:L80RE_Lora_murata-cache
LIBS:gps_l80-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ADS1231ID:ADS1231ID U7
U 1 1 5E272726
P 9950 4850
F 0 "U7" H 9950 6020 50  0000 C CNN
F 1 "ADS1231ID" H 9950 5929 50  0000 C CNN
F 2 "SOIC127P600X175-16N" H 9950 4850 50  0001 L BNN
F 3 "Texas Instruments" H 9950 4850 50  0001 L BNN
F 4 "Single Channel Single ADC Delta-Sigma 80sps 24-bit Serial 16-Pin SOIC Tube" H 9950 4850 50  0001 L BNN "Field4"
F 5 "ADS1231ID" H 9950 4850 50  0001 L BNN "Field5"
F 6 "None" H 9950 4850 50  0001 L BNN "Field6"
F 7 "Unavailable" H 9950 4850 50  0001 L BNN "Field7"
F 8 "SOIC-16 Texas Instruments" H 9950 4850 50  0001 L BNN "Field8"
	1    9950 4850
	1    0    0    -1  
$EndComp
Text HLabel 10950 4150 2    50   Output ~ 0
ADS_OUT
Text HLabel 8950 4450 0    50   Output ~ 0
ADS_SCK
$Comp
L power:GND #PWR?
U 1 1 610E5D63
P 8150 3850
AR Path="/5DE70C26/610E5D63" Ref="#PWR?"  Part="1" 
AR Path="/5E26B512/610E5D63" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/610E5D63" Ref="#PWR?"  Part="1" 
AR Path="/5FD051F0/610E5D63" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/610E5D63" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/610E5D63" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 8150 3600 50  0001 C CNN
F 1 "GND" H 8155 3677 50  0000 C CNN
F 2 "" H 8150 3850 50  0001 C CNN
F 3 "" H 8150 3850 50  0001 C CNN
	1    8150 3850
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E275BEC
P 8700 5050
AR Path="/5DE70C26/5E275BEC" Ref="C?"  Part="1" 
AR Path="/5E26B512/5E275BEC" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/5E275BEC" Ref="C?"  Part="1" 
AR Path="/5FD051F0/5E275BEC" Ref="C?"  Part="1" 
AR Path="/60054FCF/5E275BEC" Ref="C?"  Part="1" 
AR Path="/6112A4C8/5E275BEC" Ref="C64"  Part="1" 
F 0 "C64" H 8815 5096 50  0000 L CNN
F 1 "0.1uF" H 8815 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8738 4900 50  0001 C CNN
F 3 "~" H 8700 5050 50  0001 C CNN
	1    8700 5050
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 610E5D65
P 9050 3700
AR Path="/5DE70C26/610E5D65" Ref="C?"  Part="1" 
AR Path="/5E26B512/610E5D65" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/610E5D65" Ref="C?"  Part="1" 
AR Path="/5FD051F0/610E5D65" Ref="C?"  Part="1" 
AR Path="/60054FCF/610E5D65" Ref="C?"  Part="1" 
AR Path="/6112A4C8/610E5D65" Ref="C65"  Part="1" 
F 0 "C65" H 9165 3746 50  0000 L CNN
F 1 "0.1uF" H 9165 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9088 3550 50  0001 C CNN
F 3 "~" H 9050 3700 50  0001 C CNN
	1    9050 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E278566
P 8700 3850
AR Path="/5DE70C26/5E278566" Ref="C?"  Part="1" 
AR Path="/5E26B512/5E278566" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/5E278566" Ref="C?"  Part="1" 
AR Path="/5FD051F0/5E278566" Ref="C?"  Part="1" 
AR Path="/60054FCF/5E278566" Ref="C?"  Part="1" 
AR Path="/6112A4C8/5E278566" Ref="C59"  Part="1" 
F 0 "C59" H 8815 3896 50  0000 L CNN
F 1 "0.1uF" H 8815 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8738 3700 50  0001 C CNN
F 3 "~" H 8700 3850 50  0001 C CNN
	1    8700 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E279215
P 9050 3300
AR Path="/5DE70C26/5E279215" Ref="#PWR?"  Part="1" 
AR Path="/5E26B512/5E279215" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/5E279215" Ref="#PWR?"  Part="1" 
AR Path="/5FD051F0/5E279215" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/5E279215" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/5E279215" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 9050 3050 50  0001 C CNN
F 1 "GND" H 9055 3127 50  0000 C CNN
F 2 "" H 9050 3300 50  0001 C CNN
F 3 "" H 9050 3300 50  0001 C CNN
	1    9050 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	10950 4150 10850 4150
Wire Wire Line
	9050 5850 9050 5750
Wire Wire Line
	9050 5550 9050 5750
Connection ~ 9050 5750
Text HLabel 8950 5450 0    50   Output ~ 0
3V3E
Wire Wire Line
	8950 5450 9000 5450
Wire Wire Line
	9050 5050 8850 5050
Wire Wire Line
	9050 5150 8550 5150
Wire Wire Line
	8550 5150 8550 5050
Wire Wire Line
	9050 4450 8950 4450
Text HLabel 8950 4850 0    50   Output ~ 0
3V3E
Wire Wire Line
	9050 4850 8950 4850
$Comp
L power:GND #PWR?
U 1 1 610E5D68
P 9000 4950
AR Path="/5DE70C26/610E5D68" Ref="#PWR?"  Part="1" 
AR Path="/5E26B512/610E5D68" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/610E5D68" Ref="#PWR?"  Part="1" 
AR Path="/5FD051F0/610E5D68" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/610E5D68" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/610E5D68" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 9000 4700 50  0001 C CNN
F 1 "GND" H 9005 4777 50  0000 C CNN
F 2 "" H 9000 4950 50  0001 C CNN
F 3 "" H 9000 4950 50  0001 C CNN
	1    9000 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	9000 4950 9050 4950
Wire Wire Line
	9050 4550 9050 4650
$Comp
L power:GND #PWR?
U 1 1 610E5D69
P 9000 4650
AR Path="/5DE70C26/610E5D69" Ref="#PWR?"  Part="1" 
AR Path="/5E26B512/610E5D69" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/610E5D69" Ref="#PWR?"  Part="1" 
AR Path="/5FD051F0/610E5D69" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/610E5D69" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/610E5D69" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 9000 4400 50  0001 C CNN
F 1 "GND" H 9005 4477 50  0000 C CNN
F 2 "" H 9000 4650 50  0001 C CNN
F 3 "" H 9000 4650 50  0001 C CNN
	1    9000 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 4650 9000 4650
Connection ~ 9050 4650
Wire Wire Line
	9050 4650 9050 4750
Wire Wire Line
	9050 4250 9050 4150
Wire Wire Line
	8900 4150 9050 4150
Connection ~ 9050 4150
Wire Wire Line
	8150 3850 8550 3850
Wire Wire Line
	8850 3850 9050 3850
Wire Wire Line
	9050 3850 9050 4150
Connection ~ 9050 3850
Wire Wire Line
	9050 3300 9050 3550
$Comp
L power:GND #PWR?
U 1 1 610E5D6A
P 8900 5750
AR Path="/5DE70C26/610E5D6A" Ref="#PWR?"  Part="1" 
AR Path="/5E26B512/610E5D6A" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/610E5D6A" Ref="#PWR?"  Part="1" 
AR Path="/5FD051F0/610E5D6A" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/610E5D6A" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/610E5D6A" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 8900 5500 50  0001 C CNN
F 1 "GND" H 8905 5577 50  0000 C CNN
F 2 "" H 8900 5750 50  0001 C CNN
F 3 "" H 8900 5750 50  0001 C CNN
	1    8900 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	8900 5750 9050 5750
Wire Wire Line
	9050 5250 8400 5250
Wire Wire Line
	9000 5450 9000 5400
Connection ~ 9000 5450
Wire Wire Line
	9000 5450 9050 5450
Connection ~ 9050 5550
Wire Wire Line
	7700 5200 8400 5200
Wire Wire Line
	8400 5200 8400 5250
Wire Wire Line
	8350 5350 8350 5300
Wire Wire Line
	8350 5300 7700 5300
Wire Wire Line
	8350 5350 9050 5350
Wire Wire Line
	9000 5400 7700 5400
Wire Wire Line
	7700 5550 7700 5500
Wire Wire Line
	7700 5550 9050 5550
$Comp
L Device:R_Small R?
U 1 1 611C6DC5
P 4550 1850
AR Path="/5E26B512/611C6DC5" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6DC5" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6DC5" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6DC5" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6DC5" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6DC5" Ref="R28"  Part="1" 
F 0 "R28" H 4609 1896 50  0000 L CNN
F 1 "2.2R" H 4609 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4550 1850 50  0001 C CNN
F 3 "~" H 4550 1850 50  0001 C CNN
	1    4550 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 611C6DCB
P 2600 5400
AR Path="/5E26B512/611C6DCB" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6DCB" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6DCB" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6DCB" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6DCB" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6DCB" Ref="R13"  Part="1" 
F 0 "R13" H 2659 5446 50  0000 L CNN
F 1 "1k" H 2659 5355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2600 5400 50  0001 C CNN
F 3 "~" H 2600 5400 50  0001 C CNN
	1    2600 5400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 611C6DD1
P 5150 2300
AR Path="/5E26B512/611C6DD1" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/611C6DD1" Ref="C?"  Part="1" 
AR Path="/5FDDF49E/611C6DD1" Ref="C?"  Part="1" 
AR Path="/600048E7/611C6DD1" Ref="C?"  Part="1" 
AR Path="/60054FCF/611C6DD1" Ref="C?"  Part="1" 
AR Path="/6112A4C8/611C6DD1" Ref="C58"  Part="1" 
F 0 "C58" H 5265 2346 50  0000 L CNN
F 1 "100nF" H 5265 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 2150 50  0001 C CNN
F 3 "~" H 5150 2300 50  0001 C CNN
	1    5150 2300
	0    1    1    0   
$EndComp
$Comp
L mems_mic_board-cache:GND #PWR?
U 1 1 611C6DD7
P 5400 2250
AR Path="/5E26B512/611C6DD7" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/611C6DD7" Ref="#PWR?"  Part="1" 
AR Path="/5FDDF49E/611C6DD7" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/611C6DD7" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/611C6DD7" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/611C6DD7" Ref="#PWR0156"  Part="1" 
F 0 "#PWR0156" H 5400 2000 50  0001 C CNN
F 1 "GND" V 5405 2122 50  0000 R CNN
F 2 "" H 5400 2250 50  0001 C CNN
F 3 "" H 5400 2250 50  0001 C CNN
	1    5400 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 2300 5000 2300
Wire Wire Line
	5300 2300 5400 2300
Wire Wire Line
	5400 2300 5400 2250
Wire Wire Line
	2350 5400 2300 5400
Text HLabel 2300 5400 0    50   Input ~ 0
HORNEN
$Comp
L Device:C C?
U 1 1 611C6DE2
P 3950 1550
AR Path="/5E26B512/611C6DE2" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/611C6DE2" Ref="C?"  Part="1" 
AR Path="/5FDDF49E/611C6DE2" Ref="C?"  Part="1" 
AR Path="/600048E7/611C6DE2" Ref="C?"  Part="1" 
AR Path="/60054FCF/611C6DE2" Ref="C?"  Part="1" 
AR Path="/6112A4C8/611C6DE2" Ref="C33"  Part="1" 
F 0 "C33" H 4065 1596 50  0000 L CNN
F 1 "10uF 0603" H 4065 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3988 1400 50  0001 C CNN
F 3 "~" H 3950 1550 50  0001 C CNN
	1    3950 1550
	0    -1   -1   0   
$EndComp
$Comp
L mems_mic_board-cache:GND #PWR?
U 1 1 611C6DE8
P 3700 1600
AR Path="/5E26B512/611C6DE8" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/611C6DE8" Ref="#PWR?"  Part="1" 
AR Path="/5FDDF49E/611C6DE8" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/611C6DE8" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/611C6DE8" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/611C6DE8" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 3700 1350 50  0001 C CNN
F 1 "GND" V 3705 1472 50  0000 R CNN
F 2 "" H 3700 1600 50  0001 C CNN
F 3 "" H 3700 1600 50  0001 C CNN
	1    3700 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1550 4100 1550
Wire Wire Line
	3800 1550 3700 1550
Wire Wire Line
	3700 1550 3700 1600
$Comp
L Device:R_Small R?
U 1 1 611C6DF1
P 3600 2700
AR Path="/5E26B512/611C6DF1" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6DF1" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6DF1" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6DF1" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6DF1" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6DF1" Ref="R15"  Part="1" 
F 0 "R15" H 3659 2746 50  0000 L CNN
F 1 "33k" H 3659 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 3600 2700 50  0001 C CNN
F 3 "~" H 3600 2700 50  0001 C CNN
	1    3600 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 611C6DF7
P 3600 3200
AR Path="/5E26B512/611C6DF7" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6DF7" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6DF7" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6DF7" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6DF7" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6DF7" Ref="R16"  Part="1" 
F 0 "R16" H 3659 3246 50  0000 L CNN
F 1 "33k" H 3659 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 3600 3200 50  0001 C CNN
F 3 "~" H 3600 3200 50  0001 C CNN
	1    3600 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 611C6DFD
P 3150 4100
AR Path="/5E26B512/611C6DFD" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6DFD" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6DFD" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6DFD" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6DFD" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6DFD" Ref="R14"  Part="1" 
F 0 "R14" H 3209 4146 50  0000 L CNN
F 1 "33k" H 3209 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 3150 4100 50  0001 C CNN
F 3 "~" H 3150 4100 50  0001 C CNN
	1    3150 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 4100 3400 4100
$Comp
L Device:R_Small R?
U 1 1 611C6E04
P 2350 5700
AR Path="/5E26B512/611C6E04" Ref="R?"  Part="1" 
AR Path="/5F1ACB2F/611C6E04" Ref="R?"  Part="1" 
AR Path="/5FDDF49E/611C6E04" Ref="R?"  Part="1" 
AR Path="/600048E7/611C6E04" Ref="R?"  Part="1" 
AR Path="/60054FCF/611C6E04" Ref="R?"  Part="1" 
AR Path="/6112A4C8/611C6E04" Ref="R12"  Part="1" 
F 0 "R12" H 2409 5746 50  0000 L CNN
F 1 "10M" H 2409 5655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2350 5700 50  0001 C CNN
F 3 "~" H 2350 5700 50  0001 C CNN
	1    2350 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	2350 5600 2350 5400
Wire Wire Line
	4550 1550 4550 1750
Wire Wire Line
	4550 1950 4550 2300
Wire Wire Line
	4550 2300 4550 2600
Connection ~ 4550 2300
Wire Wire Line
	3850 2900 3600 2900
Wire Wire Line
	3600 2900 3600 3100
Wire Wire Line
	4550 2300 3600 2300
Wire Wire Line
	3600 2300 3600 2600
Wire Wire Line
	3600 2800 3600 2900
Connection ~ 3600 2900
$Comp
L Diode:BZX84Cxx D?
U 1 1 611C6E16
P 2800 3200
AR Path="/5E26B512/611C6E16" Ref="D?"  Part="1" 
AR Path="/5F1ACB2F/611C6E16" Ref="D?"  Part="1" 
AR Path="/5FDDF49E/611C6E16" Ref="D?"  Part="1" 
AR Path="/600048E7/611C6E16" Ref="D?"  Part="1" 
AR Path="/60054FCF/611C6E16" Ref="D?"  Part="1" 
AR Path="/6112A4C8/611C6E16" Ref="D9"  Part="1" 
F 0 "D9" V 2754 3279 50  0000 L CNN
F 1 "BZX84Cxx" V 2845 3279 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 2800 3025 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 2800 3200 50  0001 C CNN
	1    2800 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 3050 2800 2900
Wire Wire Line
	2800 2900 3600 2900
Wire Wire Line
	2800 3750 3600 3750
Wire Wire Line
	4150 3750 4150 3100
Wire Wire Line
	2800 3350 2800 3750
Wire Wire Line
	3600 3300 3600 3750
Connection ~ 3600 3750
Wire Wire Line
	3600 3750 4150 3750
$Comp
L Device:C C?
U 1 1 611C6E24
P 4300 4100
AR Path="/5E26B512/611C6E24" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/611C6E24" Ref="C?"  Part="1" 
AR Path="/5FDDF49E/611C6E24" Ref="C?"  Part="1" 
AR Path="/600048E7/611C6E24" Ref="C?"  Part="1" 
AR Path="/60054FCF/611C6E24" Ref="C?"  Part="1" 
AR Path="/6112A4C8/611C6E24" Ref="C57"  Part="1" 
F 0 "C57" H 4415 4146 50  0000 L CNN
F 1 "10nF" H 4415 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4338 3950 50  0001 C CNN
F 3 "~" H 4300 4100 50  0001 C CNN
	1    4300 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 611C6E2A
P 3700 4500
AR Path="/5E26B512/611C6E2A" Ref="C?"  Part="1" 
AR Path="/5F1ACB2F/611C6E2A" Ref="C?"  Part="1" 
AR Path="/5FDDF49E/611C6E2A" Ref="C?"  Part="1" 
AR Path="/600048E7/611C6E2A" Ref="C?"  Part="1" 
AR Path="/60054FCF/611C6E2A" Ref="C?"  Part="1" 
AR Path="/6112A4C8/611C6E2A" Ref="C32"  Part="1" 
F 0 "C32" H 3815 4546 50  0000 L CNN
F 1 "10nF" H 3815 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3738 4350 50  0001 C CNN
F 3 "~" H 3700 4500 50  0001 C CNN
	1    3700 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 4100 3400 4500
Wire Wire Line
	3400 4500 3550 4500
Wire Wire Line
	3400 4100 4150 4100
Connection ~ 3400 4100
Wire Wire Line
	3850 4500 4450 4500
Wire Wire Line
	4450 4500 4450 4100
Wire Wire Line
	4550 2800 4550 3500
Wire Wire Line
	4550 3500 5300 3500
Connection ~ 4150 3750
Wire Wire Line
	4150 5100 3400 5100
Wire Wire Line
	3400 5100 3400 5200
Connection ~ 4150 4100
Wire Wire Line
	4150 4100 4150 4800
Wire Wire Line
	5200 4500 5200 4800
Wire Wire Line
	5200 4800 4150 4800
Connection ~ 4150 4800
Wire Wire Line
	4150 4800 4150 5100
Wire Wire Line
	2350 5400 2500 5400
Wire Wire Line
	2700 5400 3100 5400
$Comp
L mems_mic_board-cache:GND #PWR?
U 1 1 611C6E4F
P 2350 5950
AR Path="/5E26B512/611C6E4F" Ref="#PWR?"  Part="1" 
AR Path="/5F1ACB2F/611C6E4F" Ref="#PWR?"  Part="1" 
AR Path="/5FDDF49E/611C6E4F" Ref="#PWR?"  Part="1" 
AR Path="/600048E7/611C6E4F" Ref="#PWR?"  Part="1" 
AR Path="/60054FCF/611C6E4F" Ref="#PWR?"  Part="1" 
AR Path="/6112A4C8/611C6E4F" Ref="#PWR0158"  Part="1" 
F 0 "#PWR0158" H 2350 5700 50  0001 C CNN
F 1 "GND" V 2355 5822 50  0000 R CNN
F 2 "" H 2350 5950 50  0001 C CNN
F 3 "" H 2350 5950 50  0001 C CNN
	1    2350 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5800 2350 5900
Wire Wire Line
	3400 5600 3400 5900
Wire Wire Line
	3400 5900 2350 5900
Connection ~ 2350 5900
Wire Wire Line
	2350 5900 2350 5950
Connection ~ 2350 5400
Wire Wire Line
	2500 2900 2800 2900
Wire Wire Line
	2500 4100 3050 4100
Connection ~ 2800 2900
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 611C6E5E
P 4750 2800
AR Path="/5E26B512/611C6E5E" Ref="J?"  Part="1" 
AR Path="/5F1ACB2F/611C6E5E" Ref="J?"  Part="1" 
AR Path="/5FDDF49E/611C6E5E" Ref="J?"  Part="1" 
AR Path="/600048E7/611C6E5E" Ref="J?"  Part="1" 
AR Path="/60054FCF/611C6E5E" Ref="J?"  Part="1" 
AR Path="/6112A4C8/611C6E5E" Ref="J10"  Part="1" 
F 0 "J10" H 4778 2826 50  0000 L CNN
F 1 "Conn_01x01_Female" H 4778 2735 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 4750 2800 50  0001 C CNN
F 3 "~" H 4750 2800 50  0001 C CNN
	1    4750 2800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:DMN10H220L Q?
U 1 1 611C6E64
P 4050 2900
AR Path="/5E26B512/611C6E64" Ref="Q?"  Part="1" 
AR Path="/5F1ACB2F/611C6E64" Ref="Q?"  Part="1" 
AR Path="/5FDDF49E/611C6E64" Ref="Q?"  Part="1" 
AR Path="/600048E7/611C6E64" Ref="Q?"  Part="1" 
AR Path="/60054FCF/611C6E64" Ref="Q?"  Part="1" 
AR Path="/6112A4C8/611C6E64" Ref="Q4"  Part="1" 
F 0 "Q4" H 4256 2946 50  0000 L CNN
F 1 "DMN10H220L" H 4256 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 2825 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMN10H220L.pdf" H 4050 2900 50  0001 L CNN
	1    4050 2900
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:DMN10H220L Q?
U 1 1 611C6E6A
P 3300 5400
AR Path="/5E26B512/611C6E6A" Ref="Q?"  Part="1" 
AR Path="/5F1ACB2F/611C6E6A" Ref="Q?"  Part="1" 
AR Path="/5FDDF49E/611C6E6A" Ref="Q?"  Part="1" 
AR Path="/600048E7/611C6E6A" Ref="Q?"  Part="1" 
AR Path="/60054FCF/611C6E6A" Ref="Q?"  Part="1" 
AR Path="/6112A4C8/611C6E6A" Ref="Q3"  Part="1" 
F 0 "Q3" H 3506 5446 50  0000 L CNN
F 1 "DMN10H220L" H 3506 5355 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3500 5325 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMN10H220L.pdf" H 3300 5400 50  0001 L CNN
	1    3300 5400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 611C6E70
P 4750 2600
AR Path="/5E26B512/611C6E70" Ref="J?"  Part="1" 
AR Path="/5F1ACB2F/611C6E70" Ref="J?"  Part="1" 
AR Path="/5FDDF49E/611C6E70" Ref="J?"  Part="1" 
AR Path="/600048E7/611C6E70" Ref="J?"  Part="1" 
AR Path="/60054FCF/611C6E70" Ref="J?"  Part="1" 
AR Path="/6112A4C8/611C6E70" Ref="J9"  Part="1" 
F 0 "J9" H 4778 2626 50  0000 L CNN
F 1 "Conn_01x01_Female" H 4778 2535 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 4750 2600 50  0001 C CNN
F 3 "~" H 4750 2600 50  0001 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 611C6E76
P 4800 2700
AR Path="/5E26B512/611C6E76" Ref="J?"  Part="1" 
AR Path="/5F1ACB2F/611C6E76" Ref="J?"  Part="1" 
AR Path="/5FDDF49E/611C6E76" Ref="J?"  Part="1" 
AR Path="/600048E7/611C6E76" Ref="J?"  Part="1" 
AR Path="/60054FCF/611C6E76" Ref="J?"  Part="1" 
AR Path="/6112A4C8/611C6E76" Ref="J11"  Part="1" 
F 0 "J11" H 4828 2726 50  0000 L CNN
F 1 "Conn_01x01_Female" H 4828 2635 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 4800 2700 50  0001 C CNN
F 3 "~" H 4800 2700 50  0001 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4100 2500 2900
Wire Wire Line
	4150 3750 4150 4100
Wire Wire Line
	4150 2700 4600 2700
$Comp
L L80RE_Lora_murata-rescue:1586863-4-stm32wb_lora_EU_card-rescue J12
U 1 1 6102BA98
P 7300 5300
F 0 "J12" H 7193 4933 50  0000 C CNN
F 1 "1586863-4-stm32wb_lora_EU_card-rescue" H 7193 5024 50  0000 C CNN
F 2 "TE_1586863-4" H 7300 5300 50  0001 L BNN
F 3 "1586863-4" H 7300 5300 50  0001 L BNN
F 4 "Compliant" H 7300 5300 50  0001 L BNN "Field4"
F 5 "https://www.te.com/usa-en/product-1586863-4.html?te_bu=Cor&te_type=disp&te_campaign=seda_glo_cor-seda-global-disp-prtnr-fy19-seda-model-bom-cta_sma-317_1&elqCampaignId=32493" H 7300 5300 50  0001 L BNN "Field5"
	1    7300 5300
	-1   0    0    1   
$EndComp
$Comp
L L80RE_Lora_murata-rescue:CPS-4013-110T-stm32wb_lora_EU_card-rescue LS1
U 1 1 6102C83E
P 5200 4300
F 0 "LS1" V 5260 4052 50  0000 R CNN
F 1 "CPS-4013-110T-stm32wb_lora_EU_card-rescue" V 5169 4052 50  0000 R CNN
F 2 "XDCR_CPS-4013-110T" H 5200 4300 50  0001 L BNN
F 3 "1.0" H 5200 4300 50  0001 L BNN
F 4 "CUI Devices" H 5200 4300 50  0001 L BNN "Field4"
F 5 "12.6 mm" H 5200 4300 50  0001 L BNN "Field5"
F 6 "Manufacturer Recommendations" H 5200 4300 50  0001 L BNN "Field6"
	1    5200 4300
	0    1    -1   0   
$EndComp
Text HLabel 4400 1550 1    50   Input ~ 0
3V3E
Text HLabel 8900 4150 0    50   Input ~ 0
3V3E
Wire Wire Line
	5100 4100 4450 4100
Connection ~ 4450 4100
Wire Wire Line
	5300 4100 5300 3500
$EndSCHEMATC
